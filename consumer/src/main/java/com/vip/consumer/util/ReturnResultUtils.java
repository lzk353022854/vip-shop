package com.vip.consumer.util;


import java.io.Serializable;

public class ReturnResultUtils implements Serializable {

    public static final Integer CODE_SUCCESS = 1;
    public static final String MSG_SUCCESS = "success";

    public static ReturnResult returnSuccess(Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(CODE_SUCCESS);
        returnResult.setMessage(MSG_SUCCESS);
        returnResult.setData(data);
        return returnResult;
    }

    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(CODE_SUCCESS);
        returnResult.setMessage(MSG_SUCCESS);
        return returnResult;
    }

    public static ReturnResult returnFail(Integer CODE_FAIL, String MSG_FAIL) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(CODE_FAIL);
        returnResult.setMessage(MSG_FAIL);
        returnResult.setData(null);
        return returnResult;
    }

}
