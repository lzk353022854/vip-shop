package com.vip.consumer.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel
public class ReturnResult<T> implements Serializable {

    private static final long serialVersionUID = 6630193577918253203L;
    @ApiModelProperty(notes = "状态码")
    private Integer code;
    @ApiModelProperty(notes = "响应信息")
    private String message;
    @ApiModelProperty(notes = "数据")
    private T data;

}
