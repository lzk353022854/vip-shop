package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.vip.common.service.VipUserService;
import com.vip.common.util.ConstantUtils;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.config.bean.WeLoginBean;
import com.vip.consumer.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 用户注册，用户登录，绑定手机号
 */

@Log4j
@Api(tags = "用户登录")
@RestController
@RequestMapping(value = "/vipUser")
public class LoginController {

    @Autowired
    private RedisUtils redisUtils;

    @Reference
    private VipUserService vipUserService;

    @Autowired
    private WeLoginBean weLoginBean;

    @Autowired
    ActiveUtils activeUtils;


    /**
     * 用户注册
     *
     * @param phone
     * @param passward
     * @return
     * @throws Exception
     */
    @ApiOperation("用户注册")
    @PostMapping(value = "/register")
    public ReturnResult register(@RequestParam(name = "手机号") String phone, @RequestParam(name = "密码") String passward) {
        if (!ObjectUtils.isEmpty(vipUserService.queryByPhone(phone))) {
            return ReturnResultUtils.returnFail(003, "用户已注册，请登录！");
        }
        try {
            vipUserService.insertUser(phone, passward);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(004, "注册失败！");
        }
    }


    /**
     * 普通登录
     *
     * @param phone
     * @param passward
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation("普通登录")
    @PostMapping(value = "/comLogin")
    public ReturnResult comLogin(@RequestParam(name = "手机号") String phone, @RequestParam(name = "密码") String passward, HttpServletRequest request) throws Exception {
        VipUserVo vipUserVo = vipUserService.queryByPhone(phone);
        if (ObjectUtils.isEmpty(vipUserVo)) {
            return ReturnResultUtils.returnFail(006, "用户未注册，请先注册！");
        } else {
            if (!vipUserService.isExistsUser(phone, passward)) {
                return ReturnResultUtils.returnFail(007, "密码错误！");
            } else {
                try {
                    String token = request.getSession().getId();
                    redisUtils.set(ConstantUtils.USER + token, JSONObject.toJSONString(vipUserVo), 3600);
                    return ReturnResultUtils.returnSuccess(token);
                } catch (Exception e) {
                    return ReturnResultUtils.returnFail(108, "登录失败！");
                }
            }
        }
    }


    /**
     * 微信登录
     *
     * @return
     */
    @ApiOperation("微信登录")
    @GetMapping(value = "/wxLogin")
    public String wxLogin() {
        String url = weLoginBean.getCodeUrl();
        return url;
    }


    /**
     * 微信回调
     *
     * @param code
     * @throws IOException
     */
    @RequestMapping(value = "/getCode")
    public void getCode(String code) throws IOException {
        String resultUrl = HttpClientUtils.doGet(weLoginBean.getUrl(code));
        JSONObject jsonObject = JSONObject.parseObject(resultUrl);
        String openid = jsonObject.getString("openid");
        if (!vipUserService.isBind(openid)) {
            log.info("------------请先绑定手机号------------");
            log.info("----------------openid---------------- " + openid);
        } else {
            VipUserVo vipUserVo = vipUserService.queryUserByOpenid(openid);
            redisUtils.set(ConstantUtils.USER + openid, JSONObject.toJSONString(vipUserVo), 3600);
            log.info("------------微信登录成功------------");
            log.info("---------------wToken:--------------" + openid);
        }
    }


    /**
     * 绑定手机号
     *
     * @param phone
     * @param openid
     * @return
     */
    @ApiOperation(value = "绑定手机号")
    @PostMapping(value = "/bindWx")
    public ReturnResult bindWx(@RequestParam(name = "手机号") String phone, @RequestParam(name = "微信openid") String openid) {
        VipUserVo vipUserVo = vipUserService.queryByPhone(phone);
        if (ObjectUtils.isEmpty(vipUserVo)) {
            return ReturnResultUtils.returnFail(109, "该手机号不存在！");
        } else if (vipUserService.isBind(openid)) {
            return ReturnResultUtils.returnFail(110, "不能重复绑定！");
        }
        try {
            vipUserVo.setOpenid(openid);
            vipUserService.bindUser(vipUserVo);
            redisUtils.set(ConstantUtils.USER + openid, JSONObject.toJSONString(vipUserVo), 3600);
            return ReturnResultUtils.returnSuccess(openid);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(111, "绑定失败！");
        }
    }


    /**
     * 测试登录
     *
     * @param vipUserVo
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "测试登录")
    @GetMapping(value = "/isLogin")
    public ReturnResult isLogin(@CurrentUser VipUserVo vipUserVo) {
        if (ObjectUtils.isEmpty(vipUserVo)) {
            return ReturnResultUtils.returnFail(112, "请先登录！");
        } else {
            VipUserVo vipUserVoNew = vipUserService.queryByPhone(vipUserVo.getPhone());
            return ReturnResultUtils.returnSuccess(vipUserVoNew);
        }
    }
}
