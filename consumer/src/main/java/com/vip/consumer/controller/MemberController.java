package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.VipUserService;
import com.vip.common.util.PageUtils;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.RedisUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 查看会员等级，兑换优惠券，查看用户优惠券
 */

@Api(tags = "会员")
@RestController
@RequestMapping(value = "/member")
public class MemberController {

    @Reference
    private VipUserService vipUserService;

    @Autowired(required = false)
    private RedisUtils redisUtils;

    /**
     * 查看会员等级
     *
     * @param vipUserVo
     * @return
     */
    @ApiOperation(value = "会员等级")
    @LoginRequired
    @GetMapping(value = "/memberLevel")
    public ReturnResult memberLevel(@CurrentUser VipUserVo vipUserVo) {
        Map<String, Object> memLevel = vipUserService.getUserLevel(vipUserVo.getPhone());
        return ReturnResultUtils.returnSuccess(memLevel);
    }

    /**
     * 使用积分兑换优惠券
     *
     * @param vipUserVo
     * @param discountId
     * @param count
     * @return
     */
    @ApiOperation(value = "积分兑换优惠券")
    @LoginRequired
    @PostMapping(value = "/pointToCoupon")
    public ReturnResult pointToCoupon(@CurrentUser VipUserVo vipUserVo, @RequestParam(name = "积分券Id") int discountId, @RequestParam(name = "兑换数量") int count) {
        //判断用户积分是否足够用于兑换
        Boolean tag = vipUserService.isEnough(vipUserVo.getPhone(), discountId, count);
        if (!tag) {
            return ReturnResultUtils.returnFail(300, "积分不足，兑换失败！");
        }
        try {
            //更新用户积分和所拥有的优惠券
            vipUserService.updDiscountToUser(discountId, vipUserVo.getPhone(), count);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(301, "兑换失败！");
        }
    }

    /**
     * 查看用户已有的积分券
     *
     * @param vipUserVo
     * @return
     */
    @ApiOperation(value = "查看已有优惠券")
    @LoginRequired
    @GetMapping(value = "/showUserDiscount")
    public ReturnResult<PageUtils> showUserDiscount(@CurrentUser VipUserVo vipUserVo, @RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "3") int pageSize) {
        try {
            PageUtils pageUtils = vipUserService.getDiscountList(vipUserVo.getPhone(), pageNo, pageSize);
            return ReturnResultUtils.returnSuccess(pageUtils);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(302, "查询失败！");
        }
    }
}
