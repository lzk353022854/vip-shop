package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.ShopCarService;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.ShopCarVo;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.PageUtils;
import com.vip.consumer.util.RedisUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "购物车")
@Transactional
@RestController
@RequestMapping(value = "/shopCar")
public class ShopCarController {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private PageUtils pageUtils;

    @Reference
    private ShopCarService shopCarService;

    @ApiOperation(value = "添加购物车")
    @LoginRequired
    @PostMapping(value = "/addShopCar")
    public ReturnResult addShopCar(@RequestParam String goodId,
                                   @CurrentUser VipUserVo vipUserVo,
                                   @ApiParam(value = "数量") @RequestParam int count) {
        return ReturnResultUtils.returnSuccess(shopCarService.addShopCar(goodId, vipUserVo.getPhone(), count));
    }

    @ApiOperation(value = "查询购物车")
    @LoginRequired
    @PostMapping(value = "/queryShopCar")
    public ReturnResult queryShopCar(@ApiParam(value = "数量") @RequestParam int pageNo,
                                     @CurrentUser VipUserVo vipUserVo) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(FinalBean.PAGESIZE);
        pageUtils.setCurrentList(shopCarService.queryShopCar(pageUtils.getPageNo(), pageUtils.getPageSize(), vipUserVo.getPhone()));
        pageUtils.setTotalCount(shopCarService.getTotalCount(vipUserVo.getPhone()));
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

    @ApiOperation(value = "删除购物车")
    @LoginRequired
    @PostMapping(value = "/deleteShopCar")
    public ReturnResult deleteShopCar(@CurrentUser VipUserVo vipUserVo,
                                      @ApiParam(value = "商品ID列表") @RequestParam String... goodsId) {
        shopCarService.deleteGoodFromShopCarList(vipUserVo.getPhone(), goodsId);
        return ReturnResultUtils.returnSuccess();
    }

    @ApiOperation(value = "修改购物车")
    @PostMapping(value = "/updateShopCar")
    public ReturnResult updateShopCar(@RequestParam String shopCarId,
                                      @ApiParam(value = "数量") @RequestParam int counts) {
        return ReturnResultUtils.returnSuccess(shopCarService.changeCountsOfGoodsFromShopCarList(shopCarId, counts));
    }

}
