package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.HistoryService;
import com.vip.common.vo.HisToryVo;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2020/6/3.
 */
@Api(tags = "搜索历史")
@RequestMapping(value = "/history")
@RestController
public class HistoryController {

    @Reference
    private HistoryService historyService;

    /**
     *
     * 查询记录增加，已存在更新，未存在增加
     *
     * @param userId 用户编号
     * @param context 查询信息
     */


    /**
     * 查询记录删除 有id根据id删，无id，删除对应用户所有信息
     *
     * @param
     * @param id 查询记录id
     */
    @ApiOperation(value = "查询记录删除")
    @LoginRequired
    @PostMapping(value = "/searchDelete")
    public void searchDelete(@CurrentUser VipUserVo vipUserVo, @RequestParam(required = false) Integer id) {
        String userId = vipUserVo.getPhone();
        historyService.deleteHistory(userId, id);
    }

    /**
     * 查看查询记录
     *
     * @param
     * @param context 查询信息，模糊查询，为空返回对应用户最近10条查询信息
     * @return
     */
    @ApiOperation(value = "查看查询记录")
    @LoginRequired
    @PostMapping(value = "/searchSelect")
    public ReturnResult searchSelect(@CurrentUser VipUserVo vipUserVo, @RequestParam(required = false) String context) {
        return ReturnResultUtils.returnSuccess(historyService.selectHistoryByTen(vipUserVo.getPhone(), context));
    }
}
