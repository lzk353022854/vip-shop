package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.OrdersService;
import com.vip.common.util.PageUtils;
import com.vip.common.vo.OrdersVo;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2020/6/4.
 */
@Api(tags = "查询订单")
@RequestMapping(value = "/order")
@RestController
public class OrdersController {
    @Reference
    private OrdersService ordersService;

    @ApiOperation(value = "查询订单")
    @LoginRequired
    @PostMapping(value = "selectOrders")
    public ReturnResult<PageUtils<List<OrdersVo>>> selectOrders(@CurrentUser VipUserVo vipUserVo, @RequestParam(required = false) Integer state, @RequestParam int pageNo) {
        return ReturnResultUtils.returnSuccess(ordersService.selectOrders(vipUserVo.getPhone(), state, pageNo));
    }
}

