package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.StoreColService;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.PageUtils;
import com.vip.consumer.util.RedisUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 店铺收藏功能：收藏店铺、查看收藏列表、删除收藏
 */
@Api(tags = "用户店铺收藏")
@RestController
@RequestMapping(value = "/storeCol")
public class StoreColController {

    @Reference
    private StoreColService storeColService;

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 收藏店铺 防止连续两次收藏/取消收藏 限制点击次数
     *
     * @param col
     * @param storeId
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "收藏店铺，收藏是1，取消收藏是-1")
    @GetMapping(value = "/colStore")
    public ReturnResult colStore(@CurrentUser VipUserVo vipUserVo,
                                 @ApiParam(value = "是否收藏") @RequestParam String col,
                                 @ApiParam(value = "店铺ID") @RequestParam String storeId) {
        if (("1".equals(col) && isCol(vipUserVo.getPhone(), storeId)) || ("-1".equals(col) && !isCol(vipUserVo.getPhone(), storeId))) {
            return ReturnResultUtils.returnFail(-200, "操作错误");
        }
        if (redisUtils.checkFreq(storeId + ":" + vipUserVo.getPhone(), 4, 2)) {
            redisUtils.lSet(storeId + ":" + vipUserVo.getPhone(), col);
            if ("1".equals(col)) {
                redisUtils.incr(FinalBean.COLSTORENUM + storeId, 1);
            } else {
                redisUtils.decr(FinalBean.COLSTORENUM + storeId, 1);
            }
            storeColService.changeStoreCol(vipUserVo.getPhone(), storeId, isCol(vipUserVo.getPhone(), storeId));
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(-200, "出现错误");
    }

    /**
     * 判断Redis中的收藏列表的和是1还是0
     *
     * @param userPhone
     * @param storeId
     * @return
     */
    private boolean isCol(@RequestParam String userPhone, @RequestParam String storeId) {
        List<Object> cols = redisUtils.lGet(storeId + ":" + userPhone, 0, -1);
        int sum = 0;
        for (Object col : cols) {
            sum += Integer.valueOf(String.valueOf(col));
        }
        return sum > 0;
    }

    /**
     * 查看用户收藏店铺
     *
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "查看用户收藏店铺")
    @GetMapping(value = "/queryStoreColList")
    public ReturnResult queryStoreColList(@CurrentUser VipUserVo vipUserVo, @ApiParam(value = "分页页码") @RequestParam int pageNo) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageSize(FinalBean.PAGESIZE);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentList(storeColService.queryStoreColList(vipUserVo.getPhone(), pageUtils.getPageNo(), pageUtils.getPageSize()));
        pageUtils.setTotalCount(storeColService.countStoreColNum(vipUserVo.getPhone()));
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

    /**
     * 删除用户收藏的店铺
     *
     * @param storeId
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "删除用户收藏的店铺")
    @GetMapping(value = "/delStoreCol")
    public ReturnResult delStoreCol(@CurrentUser VipUserVo vipUserVo, @ApiParam(value = "店铺ID") @RequestParam String storeId) {
        return ReturnResultUtils.returnSuccess(storeColService.delStoreCol(vipUserVo.getPhone(), storeId));
    }

}
