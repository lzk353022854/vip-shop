package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.TagService;
import com.vip.common.vo.ActivitiesVo;
import com.vip.consumer.param.ActivitiesParam;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "首页标签")
@RestController
@RequestMapping(value = "/tags")
public class TagController {

    @Reference
    private TagService tagService;

    @ApiOperation(value = "展示标签和活动")
    @GetMapping(value = "/showTags")
    public ReturnResult showTages() {
        return ReturnResultUtils.returnSuccess(tagService.showTags());
    }

    @ApiOperation(value = "添加活动")
    @GetMapping(value = "/addActivity")
    public ReturnResult addActivity(@Validated ActivitiesParam activitiesParam) {
        ActivitiesVo activitiesVo = new ActivitiesVo();
        BeanUtils.copyProperties(activitiesParam, activitiesVo);
        return ReturnResultUtils.returnSuccess(tagService.addActivity(activitiesVo));
    }
}
