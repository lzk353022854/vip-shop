package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.StoreService;
import com.vip.common.vo.StoresVo;
import com.vip.consumer.param.StoresParam;
import com.vip.consumer.util.PageUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "店铺")
@RestController
@RequestMapping(value = "/store")
public class StoreController {

    @Reference
    private StoreService storeService;

    @ApiOperation(value = "添加商铺")
    @GetMapping(value = "/addStore")
    private ReturnResult addStore(@Validated StoresParam storesParam) {
        StoresVo storesVo = new StoresVo();
        BeanUtils.copyProperties(storesParam, storesVo);
        return ReturnResultUtils.returnSuccess(storeService.addStore(storesVo));
    }

    @ApiOperation(value = "展示商铺")
    @GetMapping(value = "/showStore")
    private ReturnResult showStore(@ApiParam(value = "分页页码") @RequestParam int pageNo) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageSize(FinalBean.PAGESIZE);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentList(storeService.showStore(pageUtils.getPageNo(), pageUtils.getPageSize()));
        pageUtils.setTotalCount(storeService.countStore());
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

    @ApiOperation(value = "冻结商铺")
    @PostMapping(value = "/freezeStore")
    private ReturnResult freezeStore(@ApiParam(value = "商铺ID") @RequestParam String storeId) {
        return ReturnResultUtils.returnSuccess(storeService.freezeStore(storeId));
    }

    @ApiOperation(value = "解冻商铺")
    @PostMapping(value = "/thawStore")
    private ReturnResult thawStore(@ApiParam(value = "商铺ID") @RequestParam String storeId) {
        return ReturnResultUtils.returnSuccess(storeService.thawStore(storeId));
    }

    @ApiOperation(value = "修改商铺信息")
    @GetMapping(value = "/modStore")
    private ReturnResult modStore(@Validated StoresParam storesParam) {
        StoresVo storesVo = new StoresVo();
        BeanUtils.copyProperties(storesParam, storesVo);
        return ReturnResultUtils.returnSuccess(storeService.modStore(storesVo));
    }

}
