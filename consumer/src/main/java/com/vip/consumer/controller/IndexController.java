package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.IndexService;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "首页")
@RestController
@RequestMapping(value = "/index")
public class IndexController {
    @Reference
    private IndexService indexService;

    /**
     * @param tagId
     * @return
     */
    @ApiOperation(value = "活动专区")
    @GetMapping(value = "/activities")
    public ReturnResult showIndex(@RequestParam(required = false) Integer tagId) {
        // 默认首页
        tagId = tagId == null ? 1 : tagId;
        return ReturnResultUtils.returnSuccess(indexService.showIndex(tagId));
    }
}
