package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.*;
import com.vip.common.vo.*;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.common.util.PageUtils;
import com.vip.consumer.util.RedisUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品添加，商品点赞，商品收藏，商品上架，商品下架，商品详情，查看收藏商品，查看点赞商品
 */
@Api(tags = "商品")
@RestController
@RequestMapping(value = "goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @Reference
    private StoresService storesService;

    @Reference
    private GoodsLikeService goodsLikeService;

    @Reference
    private GoodsColService goodsColService;

    @Reference
    private VipUserService vipUserService;

    @Autowired
    private RedisUtils redisUtils;

    //--------------------------黄一帆------------------------

    /**
     * 商品上架，修改商品状态为1
     *
     * @param goodsIdList
     * @return
     */
    @ApiOperation(value = "商品上架")
    @PostMapping(value = "goodsOn")
    public ReturnResult goodsOn(@RequestParam List<String> goodsIdList) {
        try {
            goodsService.updGoodsStateOn(goodsIdList);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(131, "商品上架失败！");
        }
    }


    /**
     * 商品下架，修改商品状态为0
     *
     * @param goodsIdList
     * @return
     */
    @ApiOperation(value = "商品下架")
    @PostMapping(value = "goodsOff")
    public ReturnResult goodsOff(@RequestParam List<String> goodsIdList) {
        try {
            goodsService.updGoodsStateOff(goodsIdList);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(132, "商品下架失败！");
        }
    }

    /**
     * 添加商品
     *
     * @param goodsVoList
     * @return
     */
    @ApiOperation(value = "添加商品")
    @ApiImplicitParam(name = "goodsVoList", dataType = "GoodsVo")
    @PostMapping(value = "/addGoods")
    public ReturnResult addGoods(@RequestBody List<GoodsVo> goodsVoList) {
        try {
            goodsService.insertGoods(goodsVoList);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(131, "添加商品失败！");
        }
    }

    /**
     * 商品点赞,action:1为点赞，-1为取消点赞
     *
     * @param vipUserVo
     * @param goodsId
     * @param action
     * @return
     */
    @ApiOperation(value = "商品点赞")
    @LoginRequired
    @PostMapping(value = "/likeToGoods")
    public ReturnResult likeToGoods(@CurrentUser VipUserVo vipUserVo, @RequestParam(name = "商品Id") String goodsId, @RequestParam(name = "1:点赞，-1:取消点赞") String action) {
        //判断用户点赞状态
        int result = goodsService.isOkAction(true, goodsId, vipUserVo.getPhone(), action);
        switch (result) {
            case 1:
                return ReturnResultUtils.returnFail(133, "不能再次点赞！");
            case 2:
                return ReturnResultUtils.returnFail(134, "不能再次取消！");
            case 3:
                //根据action的值,更新redis和Mysql中用户对商品的点赞记录和商品的点赞数量
                goodsService.updGoodsLikeMore(goodsId, vipUserVo.getPhone(), action);
                return ReturnResultUtils.returnSuccess();
            default:
                return ReturnResultUtils.returnFail(135, "点赞失败！");
        }
    }

    /**
     * 商品收藏,action:1为收藏，-1为取消收藏
     *
     * @param vipUserVo
     * @param goodsId
     * @param action
     * @return
     */
    @ApiOperation(value = "商品收藏")
    @LoginRequired
    @PostMapping(value = "/collectToGoods")
    public ReturnResult collectToGoods(@CurrentUser VipUserVo vipUserVo, @RequestParam(name = "商品Id") String goodsId, @RequestParam(name = "1:收藏，-1:取消收藏") String action) {
        //判断用户收藏状态
        int result = goodsService.isOkAction(false, goodsId, vipUserVo.getPhone(), action);
        switch (result) {
            case 1:
                return ReturnResultUtils.returnFail(136, "不能再次收藏！");
            case 2:
                return ReturnResultUtils.returnFail(137, "不能再次取消！");
            case 3:
                //根据action的值,更新redis和Mysql中用户对商品的收藏和商品的收藏数量
                goodsService.updGoodsCollectMore(goodsId, vipUserVo.getPhone(), action);
                return ReturnResultUtils.returnSuccess();
            default:
                return ReturnResultUtils.returnFail(138, "收藏失败！");
        }
    }

    /**
     * 查看用户点赞过的商品列表
     *
     * @param vipUserVo
     * @param pageNo
     * @return
     */
    @ApiOperation(value = "查看点赞商品")
    @LoginRequired
    @PostMapping(value = "/queryLikeGoods")
    public ReturnResult<PageUtils> queryLikeGoods(@CurrentUser VipUserVo vipUserVo, @RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "3") int pageSize) {
        int totalCount = goodsService.goodsLikeNum(vipUserVo.getPhone());
        if (totalCount == 0) {
            return ReturnResultUtils.returnFail(141, "您还未点赞任何商品！");
        }
        try {
            PageUtils pageUtils = goodsService.goodsLikeByUserByPage(pageNo, pageSize, vipUserVo.getPhone(), totalCount);
            return ReturnResultUtils.returnSuccess(pageUtils);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(142, "查询失败！");
        }
    }


    /**
     * 查看用户点收藏的商品列表
     *
     * @param vipUserVo
     * @param pageNo
     * @return
     */
    @ApiOperation(value = "查看收藏商品")
    @LoginRequired
    @PostMapping(value = "/queryCollectGoods")
    public ReturnResult<PageUtils> queryCollectGoods(@CurrentUser VipUserVo vipUserVo, @RequestParam(defaultValue = "1") int pageNo, @RequestParam(defaultValue = "3") int pageSize) {
        int totalCount = goodsService.goodsColNum(vipUserVo.getPhone());
        if (totalCount == 0) {
            return ReturnResultUtils.returnFail(141, "您还未收藏任何商品！");
        }
        try {
            PageUtils pageUtils = goodsService.goodsColByUserByPage(pageNo, pageSize, vipUserVo.getPhone(), totalCount);
            return ReturnResultUtils.returnSuccess(pageUtils);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(142, "查询失败！");
        }
    }

    /**
     * 修改商品信息
     *
     * @param goodsVo
     * @return
     */
    @ApiOperation(value = "修改商品信息")
    @PostMapping(value = "/updGoods")
    public ReturnResult updGoods(@Validated GoodsVo goodsVo) {
        try {
            goodsService.updGoodsDetail(goodsVo);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(143, "修改商品信息失败！");
        }
    }

    //-----------------------------陈佩琳-------------------------

    /**
     * 精选商品，按点赞数从高到低排
     *
     * @param userId 用户编号
     * @param pageNo 页数
     * @return
     */
    @ApiOperation(value = "精选商品")
    @PostMapping(value = "/RecommendGoods")
    public ReturnResult RecommendGoods(@RequestParam(required = false) String userId, @RequestParam int pageNo) {
        PageUtils pageUtils = new PageUtils();
        pageUtils = goodsService.RecommendselectGoods(userId, pageNo, pageUtils.getPageSize());
        return ReturnResultUtils.returnSuccess(pageUtils);
    }
}
