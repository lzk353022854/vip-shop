package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.BannerService;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "首页Banner展示")
@RestController
@RequestMapping(value = "/banner")
public class BannerController {

    @Reference
    private BannerService bannerService;

    @ApiOperation(value = "展示banner列表")
    @GetMapping(value = "/showBanner")

    public ReturnResult showBanner() {
        return ReturnResultUtils.returnSuccess(bannerService.getBannerList());
    }

}
