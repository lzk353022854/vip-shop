package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.*;
import com.vip.common.vo.OrdersPayVo;
import com.vip.common.vo.OrdersVo;
import com.vip.common.vo.ShopCarVo;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.config.bean.WxPayBean;
import com.vip.consumer.service.WxPayService;
import com.vip.consumer.util.RedisUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import com.vip.consumer.util.WxPayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;

@Api(tags = "订单生成与支付")
@Transactional
@RestController
@RequestMapping(value = "/buy")
public class BuyController {

    @Reference
    private OrdersService ordersService;

    @Reference
    private GoodsService goodsService;

    @Reference
    private VipUserService vipUserService;

    @Reference
    private ShopCarService shopCarService;

    @Autowired
    private WxPayService wxPayService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private WxPayBean wxPayBean;

    //监控订单状态，订单生成后的15分钟查看一次订单状态
    @Async
    public static class Reminder {
        Timer timer;

        public Reminder(String orderPayId, OrdersService ordersService) {
            System.out.println("开始异步方法");
            timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    OrdersPayVo ordersPayVo = ordersService.checkOrderPay(orderPayId);
                    String idAll = ordersPayVo.getOrdersId();
                    List<String> idAllList = Arrays.asList(idAll.split("-"));
                    for (String orderId : idAllList) {
                        OrdersVo ordersVo = ordersService.checkOrder(orderId);
                        if (ordersVo.getState() == 1) {
                            timer.cancel();
                            return;
                        }
                        ordersService.OrdersCancelChangeState(ordersVo);
                        ordersService.payFailAddNum(ordersVo);
                    }
                    ordersService.payFailAddDiscountCount(orderPayId);
                    timer.cancel();
                    //订单生成15分钟内付款，否则取消订单
                }
                //生成订单15分钟后调用改构造方法，查询订单状态
            }, 900 * 1000);
        }
    }

    //加个登录状态验证
    @LoginRequired
    @ApiOperation(value = "订单生成与支付")
    @PostMapping(value = "/insertOrder")
    public ReturnResult insertOrder(@CurrentUser VipUserVo vipUserVo, @ApiParam(value = "购物车列表") @RequestBody List<ShopCarVo> shopCarVoList,
                                    @ApiParam(value = "优惠券编号") @RequestParam(required = false) int discountId) throws Exception {
        //验证登录成功
        if (!ObjectUtils.isEmpty(vipUserVo)) {
            //生成订单
            OrdersPayVo ordersPayVo = ordersService.insertOrder(vipUserVo, shopCarVoList, discountId);
            Reminder reminder = new Reminder(ordersPayVo.getId(), ordersService);
            if (ObjectUtils.isEmpty(ordersPayVo)) {
                return ReturnResultUtils.returnFail(-200, "生成订单出错");
            }
            //修改商品的库存
            goodsService.changeQuantity(shopCarVoList);
            //减该用户所拥有的优惠券
            ordersService.updateDiscountOfVipUser(vipUserVo, discountId, ordersPayVo.getaPay());
            //清空购物车
            String[] goodsIds = new String[shopCarVoList.size()];
            for (int i = 0; i < shopCarVoList.size(); i++) {
                goodsIds[i] = shopCarVoList.get(i).getGoodsId();
            }
            shopCarService.deleteGoodFromShopCarList(vipUserVo.getPhone(), goodsIds);
            //发送二维码
            //生成codeUrl，用来支付
            String codeUrl = wxPayService.isOrder(ordersPayVo);
            return ReturnResultUtils.returnSuccess(codeUrl);
        } else {
            return ReturnResultUtils.returnSuccess("请重新登录");
        }
    }

    //支付成功回调接口
    @RequestMapping(value = "/notifyUrl")
    public String notifyUrl(HttpServletRequest request) throws Exception {
        if (redisUtils.hasKey(FinalBean.ORDERID)) {
            return null;
        } else {
            redisUtils.set(FinalBean.ORDERID, "防止微信多次请求" + new Date(), 5000);
            //redis插入codeUrl，用来校验，防止多次重复请求
            InputStream inputStream = request.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line;
            while (null != (line = bufferedReader.readLine())) {
                sb.append(line);
            }
            bufferedReader.close();
            inputStream.close();
            Map<String, String> resultMap = WxPayUtils.xmlToMap(sb.toString());
            if (WxPayUtils.isSign(resultMap, wxPayBean.getKey())) {
                if (resultMap.get("result_code").equals("SUCCESS")) {
                    String orderPayId = resultMap.get("out_trade_no");
                    OrdersPayVo ordersPayVo = ordersService.checkOrderPay(orderPayId);
                    VipUserVo vipUserVo = vipUserService.checkVipUser(ordersPayVo.getPhone());
                    //格式化价格的小数部分，不足的补0
                    DecimalFormat decimalFormat = new DecimalFormat("0");
                    //四舍五入价格，不保留小数，实付价格乘以10为获得积分
                    int pointNum = vipUserVo.getPoint();
                    //购物赠送积分按实付金额做计算
                    vipUserVo.setPoint(pointNum + Integer.valueOf(decimalFormat.format(ordersPayVo.getaPay())));
                    //修改用户积分
                    vipUserService.changeVipUserPoint(vipUserVo);

                    String idAll = ordersPayVo.getOrdersId();
                    List<String> idAllList = Arrays.asList(idAll.split("-"));
                    idAllList.forEach(orderId -> {
                        OrdersVo ordersVo = ordersService.checkOrder(orderId);
                        ordersVo.setPayTime(new Date());
                        ordersService.changeOrderStateAndPayTime(ordersVo);
                    });
                    Map<String, String> showMap = new TreeMap<>();
                    showMap.put("return_code", "SUCCESS");
                    showMap.put("return_msg", "OK");
                    String showXml = WxPayUtils.mapToXml(showMap);
                    //业务完成自动删除
                    redisUtils.delLock(FinalBean.ORDERID);
                    //通知微信端停止发送多余请求
                    return showXml;
                } else {
                    //支付失败，商品库存复原
                    String orderPayId = resultMap.get("out_trade_no");
                    OrdersPayVo ordersPayVo = ordersService.checkOrderPay(orderPayId);
                    String idAll = ordersPayVo.getOrdersId();
                    List<String> idAllList = Arrays.asList(idAll.split("-"));
                    idAllList.forEach(orderId -> {
                        OrdersVo ordersVo = ordersService.checkOrder(orderId);
                        ordersService.payFailAddNum(ordersVo);
                    });
                    //支付失败，优惠券库存复原
                    ordersService.payFailAddDiscountCount(orderPayId);
                }
            }
            return null;
        }
    }
}
