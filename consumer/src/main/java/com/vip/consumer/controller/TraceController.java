package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.TraceService;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.PageUtils;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 足迹功能：增加足迹、分页查看足迹
 */
@Api(tags = "足迹功能")
@RestController
@RequestMapping(value = "/trace")
public class TraceController {

    @Reference
    private TraceService traceService;

    @LoginRequired
    @ApiOperation(value = "增加足迹/浏览商品")
    @GetMapping(value = "/showGood")
    public ReturnResult showGood(@CurrentUser VipUserVo vipUserVo,
                                 @ApiParam(value = "商品ID") @RequestParam String goodsId) {
        traceService.addTrace(vipUserVo.getPhone(), goodsId);
        return ReturnResultUtils.returnSuccess(traceService.showGood(vipUserVo.getPhone(), goodsId));
    }

    @LoginRequired
    @ApiOperation(value = "分页查看足迹")
    @GetMapping(value = "/showTraceList")
    public ReturnResult showTraceList(@CurrentUser VipUserVo vipUserVo, @ApiParam(value = "分页页码") @RequestParam int pageNo) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageSize(FinalBean.PAGESIZE);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentList(traceService.showTraceList(vipUserVo.getPhone(), pageUtils.getPageNo(), pageUtils.getPageSize()));
        pageUtils.setTotalCount(traceService.getTraceCount(vipUserVo.getPhone()));
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

}
