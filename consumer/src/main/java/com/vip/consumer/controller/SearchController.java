package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.GoodsService;
import com.vip.common.service.HistoryService;
import com.vip.common.service.SearchService;
import com.vip.common.service.StoreService;
import com.vip.common.util.PageUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.StoresVo;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2020/6/3.
 */
@Api(tags = "查询商品商铺")
@RequestMapping(value = "/ask")
@RestController
public class SearchController {

    @Reference
    private SearchService searchService;


    /**
     * 查询框商品
     *
     * @param name   商品名称，模糊查询
     * @param choose 排序方式 1按点赞数 2按收藏数 3按价格升序 4按价格降序
     * @param pageNo 页数
     * @return
     */
    @ApiOperation(value = "查询框商品")
    @LoginRequired
    @PostMapping(value = "/searchGoods")
    public ReturnResult searchGoods(@RequestParam String name, @RequestParam int choose, @RequestParam int pageNo, @CurrentUser VipUserVo vipUserVo) throws Exception {
        String userId = vipUserVo.getPhone();
        PageUtils pageUtils = new PageUtils();
        pageUtils = searchService.searchGoods(name, choose, pageNo, userId);
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

    /**
     * 查询框店铺
     *
     * @param name   店铺名称，模糊查询
     * @param choose 排序方式 1按相关性，店铺符合要求的商品数 2按收藏数数
     * @param pageNo 页数
     * @return
     */
    @ApiOperation(value = "查询框店铺")
    @LoginRequired
    @PostMapping(value = "/searchStores")
    public ReturnResult searchStores(@RequestParam String name, @RequestParam int choose, @RequestParam int pageNo, @CurrentUser VipUserVo vipUserVo) throws Exception {
        String userId = vipUserVo.getPhone();
        PageUtils pageUtils = new PageUtils();
        pageUtils = searchService.searchStores(name, choose, pageNo, userId);
        return ReturnResultUtils.returnSuccess(pageUtils);
    }
}
