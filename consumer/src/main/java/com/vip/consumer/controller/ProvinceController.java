package com.vip.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.ProvinceService;
import com.vip.common.vo.TAddressCityVo;
import com.vip.common.vo.TAddressProvinceVo;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = "省市区")
@RequestMapping(value = "/province")
public class ProvinceController {
    @Reference
    private ProvinceService proService;

    @ApiOperation(value = "省市区")
    @GetMapping(value = "/getPro")
    public ReturnResult getPro(@ApiParam(value = "省id") @RequestParam Integer proId, @ApiParam(value = "市id") @RequestParam(required = false) Integer cityId) {
        // 根据proId查找省份，若没有对应的省份，返回所有省份
        List<TAddressProvinceVo> provinceVos = proService.getProvince(proId);
        if (provinceVos.size() > 1) {
            return ReturnResultUtils.returnSuccess(provinceVos);
        }

        // 根据cityId查找省份对应的市和县区，若没有对应的市，返回所有市
        List<TAddressCityVo> cityVos = proService.getCity(provinceVos.get(0), cityId);
        provinceVos.get(0).setCityVos(cityVos);

        return ReturnResultUtils.returnSuccess(provinceVos);
    }
}
