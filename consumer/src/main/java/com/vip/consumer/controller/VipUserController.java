package com.vip.consumer.controller;


import com.vip.common.bean.FinalBean;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.alibaba.dubbo.config.annotation.Reference;
import com.vip.common.service.VipUserService;
import com.vip.consumer.util.RedisUtils;
import com.vip.common.util.ConstantUtils;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.config.CurrentUser;
import com.vip.consumer.config.LoginRequired;
import com.vip.consumer.util.ReturnResult;
import com.vip.consumer.util.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Timer;
import java.util.TimerTask;

@Log4j
@Api(tags = "用户信息及签到")
@EnableAsync
@RestController
@RequestMapping(value = "/vipUser")
public class VipUserController {

    @Reference
    private VipUserService vipUserService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    ApplicationContext applicationContext;

    /**
     * @param
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "基本信息")
    @GetMapping(value = "/showUserInfo")
    public ReturnResult showUserInfo(@CurrentUser VipUserVo vipUserVo) {
        return ReturnResultUtils.returnSuccess(vipUserService.showUserInfo(vipUserVo));
    }

    /**
     * @param vipUserVo
     * @return
     */
    @LoginRequired
    @ApiOperation(value = "修改基本信息")
    @PostMapping(value = "/updateUser")
    public ReturnResult updateUser(@CurrentUser VipUserVo vipUserVo, @RequestBody VipUserVo vipUserNow, HttpServletRequest request) {
        String token = request.getHeader(FinalBean.TOKEN);
        String wToken = request.getHeader(FinalBean.WTOKEN);
        if (StringUtils.isEmpty(token)) {
            token = wToken;
        }
        return ReturnResultUtils.returnSuccess(vipUserService.updateUser(vipUserVo, vipUserNow, token));
    }

    /**
     * 今日签到
     *
     * @param
     * @return
     */
    @ApiOperation(value = "今日签到")
    @LoginRequired
    @PostMapping(value = "/SignIn")
    public ReturnResult<String> SigndIn(@CurrentUser VipUserVo vipUserVo) {
        String vipUserId = vipUserVo.getPhone();
        if (null != redisUtils.get("signIn:" + vipUserId)) {
            return ReturnResultUtils.returnSuccess("签到失败，已签到");
        }
        redisUtils.set("signIn:" + vipUserId, "1");
        vipUserService.updatePoint(vipUserId, 50);
        if (null == redisUtils.get("ConSignTimes:" + vipUserId)) {
            redisUtils.set("ConSignTimes:" + vipUserId, "0");
        }
        redisUtils.set("ConSignTimes:" + vipUserId, Integer.toString(Integer.valueOf(redisUtils.get("ConSignTimes:" + vipUserId).toString()) + 1));
        vipUserService.updateSignIn(vipUserId, Integer.valueOf(redisUtils.get("ConSignTimes:" + vipUserId).toString()));
        return ReturnResultUtils.returnSuccess("完成签到");
    }

    /**
     * 签到更新，更新所有用户签到信息，每天凌晨自动调用
     *
     * @return
     */
    @Scheduled(cron = "0 0 0 1/1 * ?  ")
    @ApiOperation(value = "签到更新")
    @PostMapping(value = "/updateSignIn")
    public ReturnResult<String> updateSignIn() {
        VipUserController vipUserController = applicationContext.getBean(VipUserController.class);
        vipUserService.selectVipUserAll().forEach(vipUserVo -> {
            vipUserController.updateSignInRedis(vipUserVo.getPhone());
        });
        log.info("test1");
        return null;
    }

    /**
     * 单个用户签到信息更新，异步
     *
     * @param userId
     */
    @Async
    public void updateSignInRedis(@RequestParam String userId) {
        if (null == redisUtils.get("signIn:" + userId)) {
            if (!ObjectUtils.isEmpty(redisUtils.hasKey("ConSignTimes:" + userId))) {
                redisUtils.del("ConSignTimes:" + userId);
            }
            vipUserService.updateSignIn(userId, 0);
            return;
        }
        redisUtils.del("signIn:" + userId);
    }
}
