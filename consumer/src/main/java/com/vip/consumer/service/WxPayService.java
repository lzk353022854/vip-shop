package com.vip.consumer.service;

import com.vip.common.util.CommonUtils;
import com.vip.common.util.HttpClientUtils;
import com.vip.common.vo.OrdersPayVo;
import com.vip.common.vo.OrdersVo;
import com.vip.consumer.config.bean.WxPayBean;
import com.vip.consumer.util.WxPayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

@Service
public class WxPayService {

    @Autowired
    private WxPayBean wxPayBean;

    public String isOrder(OrdersPayVo ordersPayVo) throws Exception {
        SortedMap<String, String> param = new TreeMap<String, String>();
        //微信收款方信息
        param.put("appid", wxPayBean.getAppId());
        param.put("mch_id", wxPayBean.getMchId());
        param.put("nonce_str", UUID.randomUUID().toString().replaceAll("-", "").substring(0, 32));
        //描述,这里的body是该订单列表所含有的订单的id的字符串拼接
        param.put("body", String.valueOf(ordersPayVo.getOrdersId()));
        //订单编号(这里的订单编号不是某个订单的id，而是一个订单列表id)
        param.put("out_trade_no", ordersPayVo.getId());
        //写死，一分钱购物
        param.put("total_fee", String.valueOf(1));
        //主服务器IP
        param.put("spbill_create_ip", "192.168.2.122");
        //回调地址
        param.put("notify_url", wxPayBean.getNotifyUrl());
        //支付方式为微信支付
        param.put("trade_type", "NATIVE");
        //生成签名（一定要在最后一步）
        param.put("sign", WxPayUtils.generateSignature(param, wxPayBean.getKey()));
        //将map转换成xml样式的string去请求微信端
        String payXml = WxPayUtils.mapToXml(param);
        String resultStr = HttpClientUtils.doPost(wxPayBean.getUnifiedOrderUrl(), payXml, 5000);
        //再将微信带有code_url返回的string转换成map，以便get到code_url给前端，再用此code_url生成支付二维码
        Map<String, String> resultMap = WxPayUtils.xmlToMap(resultStr);
        return resultMap.get("code_url");
    }


}
