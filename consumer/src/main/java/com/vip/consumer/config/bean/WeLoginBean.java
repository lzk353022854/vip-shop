package com.vip.consumer.config.bean;

import com.alibaba.fastjson.JSONObject;
import com.vip.consumer.util.HttpClientUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WeLoginBean {

    private String appid;
    private String codeUri;
    private String redirect_uri;
    private String scope;
    private String accessToken;
    private String secret;
    private String userinfo;

    public String getCodeUrl() {
        StringBuffer sb = new StringBuffer(getCodeUri());
        sb.append("?").append("appid=").append(getAppid());
        sb.append("&").append("redirect_uri=").append(getRedirect_uri());
        sb.append("&").append("response_type=code");
        sb.append("&").append("scope=").append(getScope());
        sb.append("&").append("state=STATE#wechat_redirect");
        return sb.toString();
    }

    public String accToken(String code) {
        StringBuffer sbf = new StringBuffer(getAccessToken());
        sbf.append("appid=").append(getAppid());
        sbf.append("&").append("secret=").append(getSecret());
        sbf.append("&").append("code=").append(code);
        sbf.append("&").append("grant_type=authorization_code");
        return sbf.toString();
    }

    public String getUrl(String code) {
        HttpClientUtils httpClientUtils = new HttpClientUtils();
        try {
            String str = httpClientUtils.doGet(accToken(code));
            JSONObject jb = JSONObject.parseObject(str);
            StringBuffer sbff = new StringBuffer(getUserinfo());
            sbff.append("access_token=").append(jb.getString("access_token"));
            sbff.append("&").append("openid=").append(jb.getString("openid"));
            sbff.append("&").append("lang=zh_CN");
            return sbff.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "false";
    }
}
