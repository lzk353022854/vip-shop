package com.vip.consumer.config;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.vip.common.bean.FinalBean;
import com.vip.common.util.ConstantUtils;
import com.vip.common.vo.VipUserVo;
import com.vip.consumer.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class LoginRequiredComplete implements HandlerInterceptor {
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //如果方法上面没有加此注解代表不需要登录也可以访问
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        //判断接口是否登录
        LoginRequired annotation = method.getAnnotation(LoginRequired.class);
        if (null != annotation) {
            String token = request.getHeader(FinalBean.TOKEN);
            String wToken = request.getHeader(FinalBean.WTOKEN);
            if (StringUtils.isNotEmpty(token) && redisUtils.get(ConstantUtils.USER + token) != null) {
                Object obj = redisUtils.get(ConstantUtils.USER + token);
                String user_json_str = obj.toString();
                VipUserVo vipUserVo = JSONObject.parseObject(user_json_str, VipUserVo.class);
                request.setAttribute(FinalBean.LOGINUSER, vipUserVo);
            } else if (StringUtils.isNotEmpty(wToken) && redisUtils.get(ConstantUtils.USER + wToken) != null) {
                Object obj = redisUtils.get(ConstantUtils.USER + wToken);
                String user_json_str = obj.toString();
                VipUserVo vipUserVo = JSONObject.parseObject(user_json_str, VipUserVo.class);
                request.setAttribute(FinalBean.LOGINUSER, vipUserVo);
            } else {
                throw new RuntimeException("请先登录");
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
