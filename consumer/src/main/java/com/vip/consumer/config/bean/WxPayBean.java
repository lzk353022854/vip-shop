package com.vip.consumer.config.bean;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wxPay")
public class WxPayBean {

    private String appId;
    private String mchId;
    private String notifyUrl;
    private String key;
    private String unifiedOrderUrl;

}
