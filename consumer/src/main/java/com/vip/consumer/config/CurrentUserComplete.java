package com.vip.consumer.config;

import com.vip.common.vo.VipUserVo;
import org.springframework.core.MethodParameter;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class CurrentUserComplete implements HandlerMethodArgumentResolver {
    //确定是否用的是currentUser这个注解，确定是否用的是VipUserVo指定对象
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(CurrentUser.class)
                && parameter.getParameterType().isAssignableFrom(VipUserVo.class);
    }

    //计算参数值
    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        VipUserVo vipUserVo = (VipUserVo) nativeWebRequest.getAttribute("loginUser", RequestAttributes.SCOPE_REQUEST);
        if (!ObjectUtils.isEmpty(vipUserVo)) {
            return vipUserVo;
        }
        return null;
    }
}
