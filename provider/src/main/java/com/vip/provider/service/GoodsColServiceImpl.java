package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.GoodsColService;

import com.vip.common.vo.GoodsCollectionVo;

import com.vip.common.vo.GoodsVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.dto.GoodsCollection;
import com.vip.provider.dto.GoodsCollectionExample;
import com.vip.provider.mapper.GoodsCollectionMapper;
import com.vip.provider.mapper.GoodsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@Service
public class GoodsColServiceImpl implements GoodsColService {

    @Autowired
    private GoodsCollectionMapper goodsCollectionMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<GoodsVo> goodsColByUser(int pageNo, int pageSize, String phone) {
        GoodsCollectionExample goodsCollectionExample = new GoodsCollectionExample();
        goodsCollectionExample.createCriteria().andUserPhoneEqualTo(phone);
        goodsCollectionExample.setLimit(pageNo);
        goodsCollectionExample.setOffset(pageSize);
        List<GoodsCollection> goodsCollectionList = goodsCollectionMapper.selectByExample(goodsCollectionExample);
        if (goodsCollectionList.size() > 0) {
            List<GoodsVo> goodsVoList = Lists.newArrayList();
            goodsCollectionList.forEach(goodsCollection -> {
                Goods goods = goodsMapper.selectByPrimaryKey(goodsCollection.getGoodsId());
                GoodsVo goodsVo = new GoodsVo();
                BeanUtils.copyProperties(goods, goodsVo);
                goodsVoList.add(goodsVo);
            });
            return goodsVoList;
        }
        return null;
    }

    @Override
    public int goodsColNum(String phone) {
        GoodsCollectionExample goodsCollectionExample = new GoodsCollectionExample();
        goodsCollectionExample.createCriteria().andUserPhoneEqualTo(phone);
        List<GoodsCollection> goodsCollectionList = goodsCollectionMapper.selectByExample(goodsCollectionExample);
        if (goodsCollectionList != null) {
            return goodsCollectionList.size();
        }
        return 0;
    }


}
