package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.TagService;
import com.vip.common.vo.ActivitiesVo;
import com.vip.common.vo.TagVo;
import com.vip.provider.dto.Activities;
import com.vip.provider.dto.ActivitiesExample;
import com.vip.provider.dto.Tag;
import com.vip.provider.dto.TagExample;
import com.vip.provider.mapper.ActivitiesMapper;
import com.vip.provider.mapper.TagMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private ActivitiesMapper activitiesMapper;

    @Override
    public Map<TagVo, List<ActivitiesVo>> showTags() {
        Map<TagVo, List<ActivitiesVo>> resultMap = new HashMap<>();
        TagExample tagExample = new TagExample();
        tagExample.createCriteria().andStateEqualTo(1);
        List<Tag> tags = tagMapper.selectByExample(tagExample);
        tags.forEach(tag -> {
            TagVo tagVo = new TagVo();
            BeanUtils.copyProperties(tag, tagVo);
            ActivitiesExample activitiesExample = new ActivitiesExample();
            activitiesExample.createCriteria().andTagIdEqualTo(tagVo.getId());
            List<Activities> activitiesList = activitiesMapper.selectByExample(activitiesExample);
            List<ActivitiesVo> activitiesVoList = Lists.newArrayList();
            activitiesList.forEach(activities -> {
                ActivitiesVo activitiesVo = new ActivitiesVo();
                BeanUtils.copyProperties(activities, activitiesVo);
                activitiesVoList.add(activitiesVo);
            });
            resultMap.put(tagVo, activitiesVoList);
        });
        return resultMap;
    }

    @Override
    public ActivitiesVo addActivity(ActivitiesVo activitiesVo) {
        Activities activities = new Activities();
        BeanUtils.copyProperties(activitiesVo, activities);
        activitiesMapper.insertSelective(activities);
        return activitiesVo;
    }
}
