package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.GoodsLikeService;
import com.vip.common.vo.GoodsLikeVo;
import com.vip.common.vo.GoodsVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.dto.GoodsLike;
import com.vip.provider.dto.GoodsLikeExample;
import com.vip.provider.mapper.GoodsLikeMapper;
import com.vip.provider.mapper.GoodsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsLikeServiceImpl implements GoodsLikeService {

    @Autowired
    private GoodsLikeMapper goodsLikeMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public int goodsLikeNum(String phone) {
        GoodsLikeExample goodsLikeExample = new GoodsLikeExample();
        goodsLikeExample.createCriteria().andUserPhoneEqualTo(phone);
        List<GoodsLike> goodsLikeList = goodsLikeMapper.selectByExample(goodsLikeExample);
        if (goodsLikeList != null) {
            return goodsLikeList.size();
        }
        return 0;
    }

    @Override
    public List<GoodsVo> goodsLikeByUser(int pageNo, int pageSize, String phone) {
        GoodsLikeExample goodsLikeExample = new GoodsLikeExample();
        goodsLikeExample.createCriteria().andUserPhoneEqualTo(phone);
        goodsLikeExample.setLimit(pageNo);
        goodsLikeExample.setOffset(pageSize);
        List<GoodsLike> goodsLikeList = goodsLikeMapper.selectByExample(goodsLikeExample);
        if (goodsLikeList != null) {
            List<GoodsVo> goodsVoList = Lists.newArrayList();
            goodsLikeList.forEach(goodsLike -> {
                Goods goods = goodsMapper.selectByPrimaryKey(goodsLike.getGoodsId());
                GoodsVo goodsVo = new GoodsVo();
                BeanUtils.copyProperties(goods, goodsVo);
                goodsVoList.add(goodsVo);
            });
            return goodsVoList;
        }
        return null;
    }


}
