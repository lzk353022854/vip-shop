package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.vip.common.service.GoodsService;
import com.vip.common.service.HistoryService;
import com.vip.common.service.SearchService;
import com.vip.common.service.StoreService;
import com.vip.common.util.PageUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.StoresVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2020/6/5.
 */
@Service
@Transactional
public class SearchServiceImpl implements SearchService {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private HistoryService historyService;

    @Override
    public PageUtils<List<StoresVo>> searchStores(String name, int choose, int pageNo, String userId) throws Exception {
        try {
            PageUtils pageUtils = new PageUtils();
            pageUtils = storeService.selectStoreVo(name, choose, pageNo, pageUtils.getPageSize());
            if (null != userId) {
                historyService.searchAdd(userId, name);
                historyService.deleteHistoryNotInTen(userId);
            }
            return pageUtils;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public PageUtils<List<GoodsVo>> searchGoods(String name, int choose, int pageNo, String userId) throws Exception {
        try {
            PageUtils pageUtils = new PageUtils();
            pageUtils = goodsService.selectGoods(name, choose, pageNo, pageUtils.getPageSize());
            if (null != userId) {
                historyService.searchAdd(userId, name);
                historyService.deleteHistoryNotInTen(userId);
            }
            return pageUtils;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
