package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.ShopCarService;
import com.vip.common.util.CommonUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.ShopCarVo;
import com.vip.common.vo.VipUserVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.dto.GoodsExample;
import com.vip.provider.dto.ShopCar;
import com.vip.provider.dto.ShopCarExample;
import com.vip.provider.mapper.GoodsMapper;
import com.vip.provider.mapper.ShopCarMapper;
import com.vip.provider.mapper.StoresMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class ShopCarServiceImpl implements ShopCarService {

    @Autowired
    private ShopCarMapper shopCarMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private StoresMapper storesMapper;

    //根据商品id，用户信息phone和购买数量count将商品添加至该用户的购物车
    @Override
    public String addShopCar(String goodId, String phone, int count) {
        Goods goods = goodsMapper.selectByPrimaryKey(goodId);
        ShopCarExample shopCarExample = new ShopCarExample();
        shopCarExample.createCriteria().andGoodsIdEqualTo(goodId).andUserPhoneEqualTo(phone);
        List<ShopCar> shopCars = shopCarMapper.selectByExample(shopCarExample);
        //如果该商品之前已经添加过购物车，则修改该购物车的商品数量
        if (shopCars.size() == 1) {
            ShopCar shopCar = shopCars.get(0);
            if (goods.getQuantity() > shopCar.getCount() + count) {
                shopCar.setCount(shopCar.getCount() + count);
                shopCar.setUpdateTime(new Date());
                shopCarMapper.updateByPrimaryKeySelective(shopCar);
                return "添加购物车成功";
            }
            return "库存不足";
        } else {
            ShopCar shopCar = new ShopCar();
            shopCar.setId(CommonUtils.getOrderIdOrShopCarId());
            shopCar.setUserPhone(phone);
            shopCar.setGoodsId(goods.getId());
            shopCar.setStoreId(goods.getStoreId());
            if (goods.getQuantity() > count && count > 1) {
                shopCar.setCount(count);
                shopCarMapper.insertSelective(shopCar);
                return "添加购物车成功";
            } else {
                return "库存不足";
            }

        }
    }

//    //根据用户信息phone展示购物车所有商品
//    @Override
//    public List<ShopCarVo> queryShopCar(int pageNo, int pageSize, String phone) {
//        ShopCarExample shopCarExample = new ShopCarExample();
//        shopCarExample.setLimit(pageNo);
//        shopCarExample.setOffset(pageSize);
//        shopCarExample.createCriteria().andUserPhoneEqualTo(phone);
//        shopCarExample.setOrderByClause("update_time DESC");
//        List<ShopCar> shopCarList = shopCarMapper.selectByExample(shopCarExample);
//        List<ShopCarVo> shopCarVoList = Lists.newArrayList();
//        shopCarList.forEach(shopCar -> {
//            ShopCarVo shopCarVo = new ShopCarVo();
//            BeanUtils.copyProperties(shopCar, shopCarVo);
//            shopCarVoList.add(shopCarVo);
//        });
//        return shopCarVoList;
//    }


    //根据用户信息phone展示购物车所有商品
    @Override
    public List<Map<String, Object>> queryShopCar(int pageNo, int pageSize, String phone) {
        List<Map<String, Object>> resultList = Lists.newArrayList();
        ShopCarExample shopCarExample = new ShopCarExample();
        shopCarExample.setLimit(pageNo);
        shopCarExample.setOffset(pageSize);
        shopCarExample.createCriteria().andUserPhoneEqualTo(phone);
        shopCarExample.setOrderByClause("update_time DESC");
        List<ShopCar> shopCarList = shopCarMapper.selectByExample(shopCarExample);
        shopCarList.forEach(shopCar -> {
            Map<String, Object> shopCarMap = new HashMap<>();
            ShopCarVo shopCarVo = new ShopCarVo();
            BeanUtils.copyProperties(shopCar, shopCarVo);
            Goods goods = goodsMapper.selectByPrimaryKey(shopCarVo.getGoodsId());
            shopCarMap.put("购物车信息", shopCarVo);
            shopCarMap.put("商品名称", goods.getName());
            shopCarMap.put("商品单价", goods.getPrice());
            shopCarMap.put("商品图片", goods.getGoodsImage());
            shopCarMap.put("店铺名称", storesMapper.selectByPrimaryKey(goods.getStoreId()).getName());
            resultList.add(shopCarMap);
        });
        return resultList;
    }

    @Override
    public long getTotalCount(String phone) {
        ShopCarExample shopCarExample = new ShopCarExample();
        shopCarExample.createCriteria().andUserPhoneEqualTo(phone);
        long totalCount = shopCarMapper.countByExample(shopCarExample);
        return totalCount;
    }

    //根据用户信息phone和商品信息goodsId删除购物车中单个或多个商品
    @Override
    public int deleteGoodFromShopCarList(String phone, String... goodsId) {
        ShopCarExample shopCarExample = new ShopCarExample();
        List<String> goodsIdList = Lists.newArrayList(Arrays.asList(goodsId));
        shopCarExample.createCriteria().andUserPhoneEqualTo(phone).andGoodsIdIn(goodsIdList);
        int deleteCounts = shopCarMapper.deleteByExample(shopCarExample);
        //成功删除返回数量
        return deleteCounts;
    }

    //根据用户信息phone和商品信息goodsId修改购物车中商品数量
    @Override
    public String changeCountsOfGoodsFromShopCarList(String shopCarId, int counts) {
        ShopCar shopCar = shopCarMapper.selectByPrimaryKey(shopCarId);
        Goods goods = goodsMapper.selectByPrimaryKey(shopCar.getGoodsId());
        if (goods.getQuantity() >= counts && counts >= 1) {
            shopCar.setCount(counts);
            shopCar.setUpdateTime(new Date());
            int result = shopCarMapper.updateByPrimaryKeySelective(shopCar);
            if (result > 0) {
                return "修改成功";
            } else {
                return "修改失败";
            }
        }
        return "商品库存不足或者购物车商品最低购买数量为1";
    }
}

