package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.TraceService;
import com.vip.common.vo.GoodsVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.dto.Stores;
import com.vip.provider.dto.Trace;
import com.vip.provider.dto.TraceExample;
import com.vip.provider.mapper.GoodsMapper;
import com.vip.provider.mapper.StoresMapper;
import com.vip.provider.mapper.TraceMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TraceServiceImpl implements TraceService {

    @Autowired
    private TraceMapper traceMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private StoresMapper storesMapper;

    @Override
    public Map<String, Object> showGood(String userPhone, String goodId) {
        Map<String, Object> resultMap = new HashMap<>();
        // 查询商品信息
        Goods goods = goodsMapper.selectByPrimaryKey(goodId);
        GoodsVo goodsVo = new GoodsVo();
        BeanUtils.copyProperties(goods, goodsVo);

        Stores stores = storesMapper.selectByPrimaryKey(goodsVo.getStoreId());
        resultMap.put("店铺收藏", stores.getCollectCount());
        resultMap.put("商品信息", goodsVo);
        return resultMap;
    }

    @Override
    public List<GoodsVo> showTraceList(String userPhone, int pageNo, int pageSize) {
        // 获取浏览记录列表
        TraceExample traceExample = new TraceExample();
        traceExample.setLimit(pageNo);
        traceExample.setOffset(pageSize);
        traceExample.createCriteria().andUserPhoneEqualTo(userPhone);
        traceExample.setOrderByClause("scan_time DESC");
        List<Trace> traceList = traceMapper.selectByExample(traceExample);

        // 获取商品列表
        List<GoodsVo> goodsVolist = Lists.newArrayList();
        traceList.forEach(trace -> {
            Goods goods = goodsMapper.selectByPrimaryKey(trace.getGoodsId());
            GoodsVo goodsVo = new GoodsVo();
            BeanUtils.copyProperties(goods, goodsVo);
            goodsVolist.add(goodsVo);
        });
        return goodsVolist;
    }

    @Override
    public int getTraceCount(String userPhone) {
        TraceExample traceExample = new TraceExample();
        traceExample.createCriteria().andUserPhoneEqualTo(userPhone);
        return traceMapper.selectByExample(traceExample).size();
    }

    @Override
    public void addTrace(String userPhone, String goodId) {
        TraceExample traceExample = new TraceExample();
        traceExample.createCriteria().andUserPhoneEqualTo(userPhone).andGoodsIdEqualTo(goodId);
        List<Trace> traceList = traceMapper.selectByExample(traceExample);

        if (traceList.size() == 1) {
            // 如果有该记录，更新时间
            Trace trace = traceList.get(0);
            trace.setScanTime(new Date());
            traceMapper.updateByPrimaryKeySelective(trace);
        } else {
            // 如果没有该记录，添加浏览记录
            Trace trace = new Trace();
            trace.setUserPhone(userPhone);
            trace.setGoodsId(goodId);
            traceMapper.insertSelective(trace);
        }
    }
}
