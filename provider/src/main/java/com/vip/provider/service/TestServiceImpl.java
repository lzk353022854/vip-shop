package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.vip.common.service.TestService;

@Service
public class TestServiceImpl implements TestService {

    @Override
    public String getString() {
        return "123";
    }

}
