package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.BannerService;
import com.vip.common.vo.BannerVo;
import com.vip.provider.dto.Banner;
import com.vip.provider.mapper.BannerMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<BannerVo> getBannerList() {
        List<Banner> bannerList = bannerMapper.selectByExample(null);
        List<BannerVo> bannerVoList = Lists.newArrayList();
        bannerList.forEach(banner -> {
            BannerVo bannerVo = new BannerVo();
            BeanUtils.copyProperties(banner, bannerVo);
            bannerVoList.add(bannerVo);
        });
        return bannerVoList;
    }

}
