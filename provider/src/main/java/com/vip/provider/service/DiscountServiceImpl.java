package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.vip.common.service.DiscountService;
import com.vip.provider.dto.Discount;
import com.vip.provider.dto.DiscountExample;
import com.vip.provider.mapper.DiscountMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountMapper discountMapper;

    @Override
    public int getNeedPoint(int discountId) {
        Discount discount = discountMapper.selectByPrimaryKey(discountId);
        return discount.getPoint();
    }
}
