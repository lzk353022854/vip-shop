package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.vip.common.service.IndexService;
import com.vip.common.vo.ActivitiesVo;
import com.vip.common.vo.TagVo;
import com.vip.provider.dto.Activities;
import com.vip.provider.dto.ActivitiesExample;
import com.vip.provider.dto.Tag;
import com.vip.provider.dto.TagExample;
import com.vip.provider.mapper.ActivitiesMapper;
import com.vip.provider.mapper.TagMapper;
import com.vip.provider.util.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {
    @Autowired
    private ActivitiesMapper activitiesMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private RedisUtils redisUtils;

    private Integer flag;

    //展示首页信息
    @Override
    public List<TagVo> showIndex(Integer tagId) {

        String tagKey = "tags" + tagId;
        if (redisUtils.hasKey(tagKey)) {
            String tagStr = (String) redisUtils.get(tagKey);
            return JSONObject.parseArray(tagStr, TagVo.class);
        }

        // 查找所有的tag
        TagExample tagExample = new TagExample();
        List<Tag> tagDtos = tagMapper.selectByExample(tagExample);
        List<TagVo> tagVos = Lists.newArrayList();

        tagDtos.forEach(tag -> {
            TagVo tagVo = new TagVo();
            BeanUtils.copyProperties(tag, tagVo);
            tagVos.add(tagVo);

            // 根据tagId查找相应的tag
            if (tagVo.getId() == tagId) {
                flag = tagVos.indexOf(tagVo);
            }
        });

        List<ActivitiesVo> activitiesVos = Lists.newArrayList();
        ActivitiesExample example = new ActivitiesExample();
        example.createCriteria().andTagIdEqualTo(tagId);
        List<Activities> activitiesDtos = activitiesMapper.selectByExample(example);

        activitiesDtos.forEach(activities -> {
            ActivitiesVo activity = new ActivitiesVo();
            BeanUtils.copyProperties(activities, activity);
            activitiesVos.add(activity);
        });

        tagVos.get(flag).setActivitiesVo(activitiesVos);
        String tagStr = JSONObject.toJSONString(tagVos);
        redisUtils.set(tagKey, tagStr);

        return JSONObject.parseArray(tagStr, TagVo.class);
    }
}
