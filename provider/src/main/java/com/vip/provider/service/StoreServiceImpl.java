package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.StoreService;
import com.vip.common.vo.StoresVo;
import com.vip.provider.dto.Stores;
import com.vip.provider.dto.StoresExample;
import com.vip.provider.mapper.StoresMapper;
import com.vip.common.util.PageUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.terracotta.offheapstore.paging.Page;

import java.util.ArrayList;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
    private StoresMapper storesMapper;

    //---------------李振坤------------------
    @Override
    public boolean addStore(StoresVo storesVo) {
        Stores stores = new Stores();
        BeanUtils.copyProperties(storesVo, stores);
        int result = storesMapper.insertSelective(stores);
        if (result == 1) {
            return true;
        }
        return false;
    }

    @Override
    public List<StoresVo> showStore(int pageNo, int pageSize) {
        StoresExample storesExample = new StoresExample();
        storesExample.setLimit(pageNo);
        storesExample.setOffset(pageSize);
        storesExample.setOrderByClause("id DESC");
        storesExample.createCriteria().andStateEqualTo(1);
        List<Stores> storesList = storesMapper.selectByExample(storesExample);
        List<StoresVo> storesVoList = Lists.newArrayList();
        storesList.forEach(stores -> {
            StoresVo storesVo = new StoresVo();
            BeanUtils.copyProperties(stores, storesVo);
            storesVoList.add(storesVo);
        });
        return storesVoList;
    }

    @Override
    public long countStore() {
        return storesMapper.selectByExample(null).size();
    }

    @Override
    public boolean freezeStore(String storeId) {
        Stores stores = storesMapper.selectByPrimaryKey(storeId);
        if (stores.getState() == 0) {
            return false;
        }
        stores.setState(0);
        int result = storesMapper.updateByPrimaryKeySelective(stores);
        if (result == 1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean thawStore(String storeId) {
        Stores stores = storesMapper.selectByPrimaryKey(storeId);
        if (stores.getState() == 1) {
            return false;
        }
        stores.setState(1);
        int result = storesMapper.updateByPrimaryKeySelective(stores);
        if (result == 1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean modStore(StoresVo storesVo) {
        Stores stores = new Stores();
        BeanUtils.copyProperties(storesVo, stores);
        stores.setState(null);
        int result = storesMapper.updateByPrimaryKeySelective(stores);
        if (result == 1) {
            return true;
        }
        return false;
    }


    //------------------------------陈佩琳--------------------------------
    @Override
    public PageUtils<List<StoresVo>> selectStoreVo(String name, int choose, int pageNo, int pageSize) {//1是相关性查询，2是名字查询
        List<Stores> storesList = null;
        PageUtils pageUtils = new PageUtils();
        Long totalCount = null;
        long totalPage;
        pageNo = pageNo < 1 ? 1 : pageNo;
        switch (choose) {
            case 1:
                totalCount = storesMapper.getStoresByReleventCount("%" + name + "%").longValue();
                pageUtils.setTotalCount(totalCount);
                totalPage = pageUtils.getTotalPage();
                if (totalPage == 0) {
                    totalPage = 1;
                }
                pageUtils.setTotalPage(totalPage);
                pageNo = (new Long(pageNo > totalPage ? totalPage : pageNo)).intValue();
                storesList = storesMapper.getStoresByRelevent("%" + name + "%", (pageNo - 1) * pageSize, pageSize);
                break;
            case 2:
                StoresExample storesExample = new StoresExample();
                storesExample.createCriteria().andNameLike("%" + name + "%");
                totalCount = storesMapper.countByExample(storesExample);
                pageUtils.setTotalCount(totalCount);
                totalPage = pageUtils.getTotalPage();
                if (totalPage == 0) {
                    totalPage = 1;
                }
                pageUtils.setTotalPage(totalPage);
                pageNo = (new Long(pageNo > totalPage ? totalPage : pageNo)).intValue();
                storesExample.setLimit((pageNo - 1) * pageSize);
                storesExample.setOffset(pageSize);
                storesExample.setOrderByClause("`collect_count`DESC");
                storesList = storesMapper.selectByExample(storesExample);
                break;
        }
        List<StoresVo> storeVoList = new ArrayList<StoresVo>();
        storesList.forEach(stores -> {
            StoresVo storeVo = new StoresVo();
            BeanUtils.copyProperties(stores, storeVo);
            storeVoList.add(storeVo);
        });
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setCurrentList(storeVoList);
        return pageUtils;
    }


}
