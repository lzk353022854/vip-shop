package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.GoodsService;
import com.vip.common.vo.ShopCarVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.mapper.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.vip.common.util.ConstantUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.ShopCarVo;
import com.vip.provider.dto.*;
import com.vip.provider.mapper.GoodsCollectionMapper;
import com.vip.provider.mapper.GoodsLikeMapper;
import com.vip.provider.mapper.GoodsMapper;
import com.vip.provider.util.CommonUtils;
import com.vip.provider.util.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import com.vip.common.service.HistoryService;
import com.vip.common.util.PageUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.dto.GoodsExample;
import com.vip.provider.dto.History;
import com.vip.provider.dto.HistoryExample;
import com.vip.provider.mapper.GoodsMapper;
import com.vip.provider.mapper.HistoryMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import java.util.List;

@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private GoodsCollectionMapper goodsCollectionMapper;

    @Autowired
    private GoodsLikeMapper goodsLikeMapper;

    @Override
    public void changeQuantity(List<ShopCarVo> shopCarVoList) {
        shopCarVoList.forEach(shopCarVo -> {
            Goods goods = goodsMapper.selectByPrimaryKey(shopCarVo.getGoodsId());
            int quantity = goods.getQuantity();
            goods.setQuantity(quantity - shopCarVo.getCount());
            goodsMapper.updateByPrimaryKeySelective(goods);
        });
    }

    //------------------------------------黄一帆-----------------------------------

    /**
     * 批量添加商品
     *
     * @param goodsVoList
     */
    @Override
    public void insertGoods(List<GoodsVo> goodsVoList) {
        goodsVoList.forEach(goodsVo -> {
            Goods goods = new Goods();
            goodsVo.setId(CommonUtils.getUUID().substring(0, 10));
            BeanUtils.copyProperties(goodsVo, goods);
            goodsMapper.insertSelective(goods);
        });
    }


    @Override
    public void updGoodsStateOn(List<String> goodsIdList) {
        goodsIdList.forEach(goodsId -> {
            Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
            goods.setState(1);
            goodsMapper.updateByPrimaryKey(goods);
        });
    }


    @Override
    public void updGoodsStateOff(List<String> goodsIdList) {
        goodsIdList.forEach(goodsId -> {
            Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
            goods.setState(0);
            goodsMapper.updateByPrimaryKey(goods);
        });
    }

    /**
     * 修改商品的点赞量
     *
     * @param goodsId
     */
    @Override
    public void updGoodsLike(String goodsId, boolean flag) {
        Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
        if (flag) {
            goods.setLike(goods.getLike() + 1);
        } else {
            goods.setLike(goods.getLike() - 1);
        }
        goodsMapper.updateByPrimaryKey(goods);
    }

    /**
     * 修改商品的收藏量
     *
     * @param goodsId
     */
    @Override
    public void updGoodsCollect(String goodsId, boolean flag) {
        Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
        if (flag) {
            goods.setCollection(goods.getCollection() + 1);
        } else {
            goods.setCollection(goods.getCollection() - 1);
        }
        goodsMapper.updateByPrimaryKey(goods);
    }

    /**
     * 更新redis及mysql中用户对商品的收藏记录、商品的收藏量
     *
     * @param goodsId
     * @param phone
     * @param action
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updGoodsCollectMore(String goodsId, String phone, String action) {
        //更新redis中用户对商品的收藏记录，-1为取消收藏，1为收藏
        redisUtils.lSet(ConstantUtils.COLLECT + ":" + goodsId + ":" + phone, action);
        if ("1".equals(action)) {

            //redis中对商品的收藏数量自增1
            redisUtils.incr(ConstantUtils.COLLECTNUM + goodsId, 1);

            //向mysql中插入用户对商品的收藏记录
            GoodsCollection goodsCollection = new GoodsCollection();

            goodsCollection.setGoodsId(goodsId);
            goodsCollection.setUserPhone(phone);
            goodsCollectionMapper.insertSelective(goodsCollection);

            //增加mysql中商品的收藏数量
            updGoodsCollect(goodsId, true);
        } else {
            //redis中对商品的收藏数量自减1
            redisUtils.decr(ConstantUtils.COLLECTNUM + goodsId, 1);

            ////从mysql中删除用户对商品的收藏记录
            GoodsCollectionExample goodsCollectionExample = new GoodsCollectionExample();
            goodsCollectionExample.createCriteria().andGoodsIdEqualTo(goodsId);
            goodsCollectionExample.createCriteria().andUserPhoneEqualTo(phone);
            List<GoodsCollection> goodsCollectionList = goodsCollectionMapper.selectByExample(goodsCollectionExample);
            GoodsCollection goodsCollection = goodsCollectionList.get(0);
            goodsCollectionMapper.deleteByPrimaryKey(goodsCollection.getId());

            //减少mysql中商品的收藏数量
            updGoodsCollect(goodsId, false);
        }
    }

    /**
     * 更新redis及mysql中用户对商品的点赞记录、商品的点赞量
     *
     * @param goodsId
     * @param phone
     * @param action
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updGoodsLikeMore(String goodsId, String phone, String action) {
        //更新redis中用户对商品的点赞记录，-1为取消点赞，1为点赞
        redisUtils.lSet(ConstantUtils.LIKE + ":" + goodsId + ":" + phone, action);
        if ("1".equals(action)) {

            //redis中对商品的点赞数量自增1
            redisUtils.incr(ConstantUtils.LIKENUM + goodsId, 1);

            //向mysql中插入用户对商品的点赞记录
            GoodsLike goodsLike = new GoodsLike();

            goodsLike.setGoodsId(goodsId);
            goodsLike.setUserPhone(phone);
            goodsLikeMapper.insertSelective(goodsLike);

            //增加mysql中商品的点赞数量
            updGoodsLike(goodsId, true);
        } else {
            //redis中对商品的点赞数量自减1
            redisUtils.decr(ConstantUtils.LIKENUM + goodsId, 1);

            //从mysql中删除用户对商品的点赞记录
            GoodsLikeExample goodsLikeExample = new GoodsLikeExample();
            goodsLikeExample.createCriteria().andGoodsIdEqualTo(goodsId);
            goodsLikeExample.createCriteria().andUserPhoneEqualTo(phone);
            List<GoodsLike> goodsLikeList = goodsLikeMapper.selectByExample(goodsLikeExample);
            GoodsLike goodsLike = goodsLikeList.get(0);
            goodsLikeMapper.deleteByPrimaryKey(goodsLike.getId());

            //减少mysql中商品的点赞数量
            updGoodsLike(goodsId, false);
        }
    }

    /**
     * 判断用户的点赞/收藏状态
     *
     * @param flag
     * @param goodsId
     * @param phone
     * @param action
     * @return
     */
    @Override
    public int isOkAction(boolean flag, String goodsId, String phone, String action) {
        int sum = 0;
        String key;

        //通过flag判断点赞或收藏
        if (flag) {
            key = ConstantUtils.LIKE;
        } else {
            key = ConstantUtils.COLLECT;
        }

        //对redis保存用户点赞状态的list求和，大于0则为点赞/收藏状态，小于等于0则为取消点赞/收藏状态
        List<Object> list = redisUtils.lGet(key + ":" + goodsId + ":" + phone, 0, -1);
        for (Object o : list) {
            sum += Integer.parseInt(String.valueOf(o));
        }

        //根据sum和action判断用户是否能正常操作
        if (sum > 0 && "1".equals(action)) {
            return 1;
        } else if (sum <= 0 && "-1".equals(action)) {
            return 2;
        }
        return 3;
    }

    /**
     * 分页查询用户收藏的商品
     *
     * @param pageNo
     * @param pageSize
     * @param phone
     * @return
     */
    @Override
    public List<GoodsVo> goodsColByUser(int pageNo, int pageSize, String phone) {
        GoodsCollectionExample goodsCollectionExample = new GoodsCollectionExample();
        goodsCollectionExample.createCriteria().andUserPhoneEqualTo(phone);
        goodsCollectionExample.setLimit(pageNo);
        goodsCollectionExample.setOffset(pageSize);
        List<GoodsCollection> goodsCollectionList = goodsCollectionMapper.selectByExample(goodsCollectionExample);
        if (!ObjectUtils.isEmpty(goodsCollectionList)) {
            List<GoodsVo> goodsVoList = new ArrayList<>();
            for (GoodsCollection goodsCollection : goodsCollectionList) {
                Goods goods = goodsMapper.selectByPrimaryKey(goodsCollection.getGoodsId());
                GoodsVo goodsVo = new GoodsVo();
                BeanUtils.copyProperties(goods, goodsVo);
                goodsVoList.add(goodsVo);
            }
            return goodsVoList;
        }
        return null;
    }

    /**
     * 分页查询用户点赞的商品
     *
     * @param pageNo
     * @param pageSize
     * @param phone
     * @return
     */
    @Override
    public List<GoodsVo> goodsLikeByUser(int pageNo, int pageSize, String phone) {
        GoodsLikeExample goodsLikeExample = new GoodsLikeExample();
        goodsLikeExample.createCriteria().andUserPhoneEqualTo(phone);
        goodsLikeExample.setLimit(pageNo);
        goodsLikeExample.setOffset(pageSize);
        List<GoodsLike> goodsLikeList = goodsLikeMapper.selectByExample(goodsLikeExample);
        if (!ObjectUtils.isEmpty(goodsLikeList)) {
            List<GoodsVo> goodsVoList = new ArrayList<>();
            for (GoodsLike goodsLike : goodsLikeList) {
                Goods goods = goodsMapper.selectByPrimaryKey(goodsLike.getGoodsId());
                GoodsVo goodsVo = new GoodsVo();
                BeanUtils.copyProperties(goods, goodsVo);
                goodsVoList.add(goodsVo);
            }
            return goodsVoList;
        }
        return null;
    }

    /**
     * 查询用户收藏的商品数量
     *
     * @param phone
     * @return
     */
    @Override
    public int goodsColNum(String phone) {
        GoodsCollectionExample goodsCollectionExample = new GoodsCollectionExample();
        goodsCollectionExample.createCriteria().andUserPhoneEqualTo(phone);
        List<GoodsCollection> goodsCollectionList = goodsCollectionMapper.selectByExample(goodsCollectionExample);
        if (!ObjectUtils.isEmpty(goodsCollectionList)) {
            return goodsCollectionList.size();
        }
        return 0;
    }

    /**
     * 查询用户点赞的商品数量
     *
     * @param phone
     * @return
     */
    @Override
    public int goodsLikeNum(String phone) {
        GoodsLikeExample goodsLikeExample = new GoodsLikeExample();
        goodsLikeExample.createCriteria().andUserPhoneEqualTo(phone);
        List<GoodsLike> goodsLikeList = goodsLikeMapper.selectByExample(goodsLikeExample);
        if (!ObjectUtils.isEmpty(goodsLikeList)) {
            return goodsLikeList.size();
        }
        return 0;
    }

    /**
     * 对分页查询用户收藏的商品结果封装
     *
     * @param pageNo
     * @param pageSize
     * @param phone
     * @param totalCount
     * @return
     */
    @Override
    public PageUtils goodsColByUserByPage(int pageNo, int pageSize, String phone, int totalCount) {
        PageUtils pageUtils = getPageUtils(pageNo, pageSize, totalCount);
        List<GoodsVo> goodsVoList = goodsColByUser(pageUtils.getPageNo(), pageSize, phone);
        pageUtils.setCurrentList(goodsVoList);
        return pageUtils;
    }

    /**
     * 对分页查询用户点赞的商品结果封装
     *
     * @param pageNo
     * @param pageSize
     * @param phone
     * @param totalCount
     * @return
     */
    @Override
    public PageUtils goodsLikeByUserByPage(int pageNo, int pageSize, String phone, int totalCount) {
        PageUtils pageUtils = getPageUtils(pageNo, pageSize, totalCount);
        List<GoodsVo> goodsVoList = goodsLikeByUser(pageUtils.getPageNo(), pageSize, phone);
        pageUtils.setCurrentList(goodsVoList);
        return pageUtils;
    }

    /**
     * 分页
     *
     * @param pageNo
     * @param pageSize
     * @param totalCount
     * @return
     */
    public PageUtils getPageUtils(int pageNo, int pageSize, int totalCount) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageSize(pageSize);
        pageUtils.setTotalCount(totalCount);
        if (pageNo <= 0) {
            pageNo = 1;
        } else if (pageNo > pageUtils.getTotalPage()) {
            pageNo = (int) pageUtils.getTotalPage();
        }
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentPage(pageNo);
        return pageUtils;
    }

    /**
     * 修改商品信息
     *
     * @param goodsVo
     */
    @Override
    public void updGoodsDetail(GoodsVo goodsVo) {
        Goods goods = new Goods();
        BeanUtils.copyProperties(goodsVo, goods);
        goodsMapper.updateByPrimaryKeySelective(goods);
    }

    //------------------------------------陈佩琳-----------------------------------
//    @Override
    public PageUtils<List<GoodsVo>> selectGoods(String name, int choose, int pageNo, int pageSize) {//记得加分页
        GoodsExample goodsExample = new GoodsExample();
        switch (choose) {
            case 1:
                goodsExample.createCriteria().andNameLike("%" + name + "%").andStateEqualTo(1);//要是上架商品
                goodsExample.setOrderByClause("`like`DESC");
                break;
            case 2:
                goodsExample.createCriteria().andNameLike("%" + name + "%").andStateEqualTo(1);
                goodsExample.setOrderByClause("`collection`DESC");
                break;
            case 3:
                goodsExample.createCriteria().andNameLike("%" + name + "%").andStateEqualTo(1);
                goodsExample.setOrderByClause("`price`ASC");
                break;
            case 4:
                goodsExample.createCriteria().andNameLike("%" + name + "%").andStateEqualTo(1);
                goodsExample.setOrderByClause("`price`DESC");
                break;
        }
        PageUtils pageUtils = new PageUtils();
        long totalCount = goodsMapper.countByExample(goodsExample);
        pageNo = pageNo < 1 ? 1 : pageNo;
        pageUtils.setTotalCount(totalCount);
        long totalPage = pageUtils.getTotalPage();
        if (totalPage == 0) {
            totalPage = 1;
        }
        pageUtils.setTotalPage(totalPage);
        pageNo = (new Long(pageNo > totalPage ? totalPage : pageNo)).intValue();
        goodsExample.setLimit((pageNo - 1) * pageSize);
        goodsExample.setOffset(pageSize);
        List<Goods> goodsList = goodsMapper.selectByExample(goodsExample);
        List<GoodsVo> goodsVoList = new ArrayList<GoodsVo>();
        goodsList.forEach(goods -> {
            GoodsVo goodsVo = new GoodsVo();
            BeanUtils.copyProperties(goods, goodsVo);
            goodsVoList.add(goodsVo);
        });
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setCurrentList(goodsVoList);
        return pageUtils;
    }


    /**
     * @param userId
     * @param pageNo
     * @param pageSize
     * @return
     */
//    @Override
    public PageUtils RecommendselectGoods(String userId, int pageNo, int pageSize) {
        GoodsExample goodsExample = new GoodsExample();
        PageUtils pageUtils = new PageUtils();
        pageNo = pageNo < 1 ? 1 : pageNo;
        long totalCount = goodsMapper.countByExample(goodsExample);
        pageUtils.setTotalCount(totalCount);
        long totalPage = pageUtils.getTotalPage();
        if (totalPage == 0) {
            totalPage = 1;
        }
        pageUtils.setTotalPage(totalPage);
        pageNo = (new Long(pageNo > totalPage ? totalPage : pageNo)).intValue();
        goodsExample.setLimit((pageNo - 1) * pageSize);
        goodsExample.setOffset(pageSize);
        goodsExample.setOrderByClause("`like` DESC");
        List<Goods> goodsList = goodsMapper.selectByExample(goodsExample);
        List<GoodsVo> goodsVoList = new ArrayList<GoodsVo>();
        goodsList.forEach(goods -> {
            GoodsVo goodsVo = new GoodsVo();
            BeanUtils.copyProperties(goods, goodsVo);
            goodsVoList.add(goodsVo);
        });
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setCurrentList(goodsVoList);
        return pageUtils;
    }
}
