package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.service.ProvinceService;
import com.vip.common.vo.TAddressCityVo;
import com.vip.common.vo.TAddressProvinceVo;
import com.vip.common.vo.TAddressTownVo;
import com.vip.provider.dto.*;
import com.vip.provider.mapper.TAddressCityMapper;
import com.vip.provider.mapper.TAddressProvinceMapper;
import com.vip.provider.mapper.TAddressTownMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class ProvinceImpl implements ProvinceService {
    @Autowired
    private TAddressProvinceMapper proMapper;

    @Autowired
    private TAddressCityMapper cityMapper;

    @Autowired
    private TAddressTownMapper townMapper;

    // 查找省份
    @Override
    public List<TAddressProvinceVo> getProvince(Integer proId) {
        List<TAddressProvince> provinceDtos = Lists.newArrayList();
        List<TAddressProvinceVo> provinceVos = Lists.newArrayList();
        TAddressProvince proDto = new TAddressProvince();

        proDto = proMapper.selectByPrimaryKey(proId);

        if (proDto != null) {
            provinceDtos.add(proDto);
        } else {
            TAddressProvinceExample proExample = new TAddressProvinceExample();
            provinceDtos = proMapper.selectByExample(proExample);
        }

        provinceDtos.forEach(province -> {
            TAddressProvinceVo proVo = new TAddressProvinceVo();
            BeanUtils.copyProperties(province, proVo);
            provinceVos.add(proVo);
        });

        return provinceVos;
    }

    // 查找市
    @Override
    public List<TAddressCityVo> getCity(TAddressProvinceVo proVo, Integer cityId) {
        TAddressCityExample cityExample = new TAddressCityExample();
        List<TAddressCity> cityDtos = Lists.newArrayList();
        List<TAddressCityVo> cityVos = Lists.newArrayList();

        cityExample.createCriteria().andProvincecodeEqualTo(proVo.getCode());
        cityDtos = cityMapper.selectByExample(cityExample);

        for (TAddressCity city : cityDtos) {
            TAddressCityVo cityVo = new TAddressCityVo();
            BeanUtils.copyProperties(city, cityVo);
            if (city.getId() == cityId) {
                cityVo = getTown(cityVo);
                cityVos.clear();
                cityVos.add(cityVo);
                break;
            }
            cityVos.add(cityVo);
        }

        return cityVos;
    }

    // 查找县区
    public TAddressCityVo getTown(TAddressCityVo cityVo) {
        TAddressTownExample townExample = new TAddressTownExample();
        List<TAddressTown> townDtos = Lists.newArrayList();
        List<TAddressTownVo> townVos = Lists.newArrayList();

        townExample.createCriteria().andCitycodeEqualTo(cityVo.getCode());
        townDtos = townMapper.selectByExample(townExample);

        townDtos.forEach(town -> {
            TAddressTownVo townVo = new TAddressTownVo();
            BeanUtils.copyProperties(town, townVo);
            townVos.add(townVo);
        });
        cityVo.setTown(townVos);
        return cityVo;
    }
}
