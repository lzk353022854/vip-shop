package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.HistoryService;
import com.vip.common.vo.HisToryVo;
import com.vip.provider.dto.History;
import com.vip.provider.dto.HistoryExample;
import com.vip.provider.mapper.HistoryMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2020/6/3.
 */
@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryMapper historyMapper;

    /**
     * @param hisToryVo
     * @return
     */
    @Override
    public boolean updateHistory(HisToryVo hisToryVo) {
        try {
            HistoryExample historyExample = new HistoryExample();
            historyExample.createCriteria().andIdEqualTo(hisToryVo.getId());
            History history = new History();
            BeanUtils.copyProperties(hisToryVo, history);
            history.setSearchTime(new Date());
            historyMapper.updateByExample(history, historyExample);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param userId
     * @param historyId
     * @return
     */
    @Override
    public boolean deleteHistory(String userId, Integer historyId) {
        try {
            HistoryExample historyExample = new HistoryExample();
            if (null == historyId) {
                historyExample.createCriteria().andUserPhoneEqualTo(userId);
            } else {
                historyExample.createCriteria().andUserPhoneEqualTo(userId).andIdEqualTo(historyId);
            }
            historyMapper.deleteByExample(historyExample);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param hisToryVo
     * @return
     */
    @Override
    public boolean addHistory(HisToryVo hisToryVo) {
        try {
            History history = new History();
            BeanUtils.copyProperties(hisToryVo, history);
            historyMapper.insert(history);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * *
     *
     * @param userId
     * @param name
     * @return
     */
    @Override
    public List<HisToryVo> selectHistoryByTen(String userId, String name) {//记得加限制，10条
        HistoryExample historyExample = new HistoryExample();
        historyExample.setLimit(FinalBean.SEARCHSIZE);
        historyExample.setOrderByClause("search_time DESC");
        if (null != name) {
            historyExample.createCriteria().andContentLike("%" + name + "%").andUserPhoneEqualTo(userId);
        } else {
            historyExample.createCriteria().andUserPhoneEqualTo(userId);
        }
        List<History> historyList = historyMapper.selectByExample(historyExample);
        List<HisToryVo> hisToryVoList = new ArrayList<HisToryVo>();
        historyList.forEach(history -> {
            HisToryVo hisToryVo = new HisToryVo();
            BeanUtils.copyProperties(history, hisToryVo);
            hisToryVoList.add(hisToryVo);
        });
        return hisToryVoList;
    }

    /**
     * @param userId
     * @param context
     * @return
     */
    public HisToryVo isExistsByContextAndUserId(String userId, String context) {
        HistoryExample historyExample = new HistoryExample();
        historyExample.createCriteria().andContentEqualTo(context).andUserPhoneEqualTo(userId);
        List<History> historyList = historyMapper.selectByExample(historyExample);
        if (0 != historyList.size()) {
            HisToryVo hisToryVo = new HisToryVo();
            BeanUtils.copyProperties(historyList.get(0), hisToryVo);
            return hisToryVo;
        }
        return null;
    }

    public boolean searchAdd(@RequestParam String userId, @RequestParam String context) {
        try {
            HisToryVo hisToryVo = isExistsByContextAndUserId(userId, context);
            if (null != hisToryVo) {
                updateHistory(hisToryVo);
                return true;
            }
            hisToryVo = new HisToryVo();
            hisToryVo.setContent(context);
            hisToryVo.setSearchTime(new Date());
            hisToryVo.setUserPhone(userId);
            addHistory(hisToryVo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteHistoryNotInTen(String userId) {
        try {
            HistoryExample historyExample = new HistoryExample();
            historyExample.createCriteria().andUserPhoneEqualTo(userId);
            long count = historyMapper.countByExample(historyExample);
            if (count > 10) {
                historyExample.setOrderByClause("`search_time`ASC");
                int id = historyMapper.selectByExample(historyExample).get(0).getId();
                historyMapper.deleteByPrimaryKey(id);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
