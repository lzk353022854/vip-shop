package com.vip.provider.service;


import com.vip.common.util.PageUtils;
import com.vip.common.vo.UserDiscountVo;
import com.vip.provider.dto.*;
import com.vip.provider.mapper.DiscountMapper;
import com.vip.provider.mapper.UserDiscountMapper;
import com.vip.provider.util.MD5Utils;
import lombok.extern.log4j.Log4j;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.vip.common.service.VipUserService;
import com.vip.common.util.ConstantUtils;
import com.vip.common.vo.VipUserVo;
import com.vip.provider.dto.VipUserExample;
import com.vip.provider.mapper.VipUserMapper;
import com.vip.provider.util.DataIntegrityUtils;
import com.vip.provider.util.MD5Utils;
import com.vip.provider.util.RedisUtils;
import com.vip.provider.util.MD5Utils;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j
@Service
@Transactional
public class VipUserServiceImpl implements VipUserService {

    @Autowired
    private VipUserMapper vipUserMapper;

    @Autowired
    private DataIntegrityUtils dataIntegrityUtils;

    @Autowired
    private UserDiscountMapper userDiscountMapper;

    @Autowired
    private DiscountMapper discountMapper;


    @Autowired
    private RedisUtils redisUtils;


    //---------------------李誉------------------------
    @Override
    public VipUserVo checkVipUser(String phone) {
        VipUser vipUser = vipUserMapper.selectByPrimaryKey(phone);
        VipUserVo vipUserVo = new VipUserVo();
        BeanUtils.copyProperties(vipUser, vipUserVo);
        return vipUserVo;
    }

    @Override
    public void changeVipUserPoint(VipUserVo vipUserVo) {
        VipUser vipUser = new VipUser();
        BeanUtils.copyProperties(vipUserVo, vipUser);
        vipUserMapper.updateByPrimaryKeySelective(vipUser);
    }

    //----------------------陈佩琳---------------------
    @Override
    public boolean updatePoint(String userId, int point) {
        try {
            vipUserMapper.updatePoint(point, userId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean updateSignIn(String userId, int num) {//凌晨更新,积分和天数是否要进行事务处理，积分要放在Redis中吗
        try {
            VipUserExample vipUserExample = new VipUserExample();
            vipUserExample.createCriteria().andPhoneEqualTo(userId);
            VipUser vipUser = vipUserMapper.selectByExample(vipUserExample).get(0);
            vipUser.setConSignTimes(num);
            vipUserMapper.updateByExample(vipUser, vipUserExample);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @return
     */
    @Override
    public List<VipUserVo> selectVipUserAll() {
        VipUserExample vipUserExample = new VipUserExample();
        List<VipUser> vipUsers = vipUserMapper.selectByExample(vipUserExample);
        List<VipUserVo> vipUserVoList = new ArrayList<VipUserVo>();
        vipUsers.forEach(vipUser -> {
            VipUserVo vipUserVo = new VipUserVo();
            BeanUtils.copyProperties(vipUser, vipUserVo);
            vipUserVoList.add(vipUserVo);
        });
        return vipUserVoList;
    }

    //---------------------黄一帆--------------------

    /**
     * 查询用户当前积分
     *
     * @param phone
     * @return
     */
    @Override
    public int getUserPoint(String phone) {
        VipUser vipUser = vipUserMapper.selectByPrimaryKey(phone);
        VipUserVo vipUserVo = new VipUserVo();
        if (!ObjectUtils.isEmpty(vipUser)) {
            BeanUtils.copyProperties(vipUser, vipUserVo);
            return vipUserVo.getPoint();
        } else {
            return 0;
        }
    }

    /**
     * 向mysql中插入新用户，密码采用MD5加密
     *
     * @param phone
     * @param passward
     * @throws Exception
     */
    @Override
    public void insertUser(String phone, String passward) throws Exception {
        VipUser vipUser = new VipUser();
        vipUser.setPhone(phone);
        //对密码进行MD5加密
        String newPwd = MD5Utils.getMD5Str(passward);
        vipUser.setPassword(newPwd);
        vipUserMapper.insertSelective(vipUser);
    }

    /**
     * 根据手机号和密码判断用户是否存在
     *
     * @param phone
     * @param passward
     * @return
     * @throws Exception
     */
    @Override
    public boolean isExistsUser(String phone, String passward) throws Exception {
        VipUser vipUser = vipUserMapper.selectByPrimaryKey(phone);
        //验证密码是否正确
        if (MD5Utils.getMD5Str(passward).equals(vipUser.getPassword())) {
            return true;
        }
        return false;
    }

    /**
     * 判断微信是否绑定手机号
     *
     * @param openid
     * @return
     */
    @Override
    public boolean isBind(String openid) {
        VipUserExample vipUserExample = new VipUserExample();
        vipUserExample.createCriteria().andOpenidEqualTo(openid);
        List<VipUser> vipUserList = vipUserMapper.selectByExample(vipUserExample);
        if (!ObjectUtils.isEmpty(vipUserList)) {
            return true;
        }
        return false;
    }

    /**
     * 微信号绑定用户
     *
     * @param vipUserVo
     */
    @Override
    public void bindUser(VipUserVo vipUserVo) {
        VipUser vipUser = new VipUser();
        BeanUtils.copyProperties(vipUserVo, vipUser);
        vipUserMapper.updateByPrimaryKey(vipUser);
    }

    /**
     * 根据openid查找绑定微信账号的用户信息
     *
     * @param openid
     * @return
     */
    @Override
    public VipUserVo queryUserByOpenid(String openid) {
        VipUserExample vipUserExample = new VipUserExample();
        vipUserExample.createCriteria().andOpenidEqualTo(openid);
        List<VipUser> vipUserList = vipUserMapper.selectByExample(vipUserExample);
        List<VipUserVo> vipUserVoList = new ArrayList<>();
        for (VipUser vipUser : vipUserList) {
            VipUserVo vipUserVo = new VipUserVo();
            BeanUtils.copyProperties(vipUser, vipUserVo);
            vipUserVoList.add(vipUserVo);
        }
        return vipUserVoList.get(0);
    }

    /**
     * 根据用户id查找用户
     *
     * @param phone
     * @return
     */
    @Override
    public VipUserVo queryByPhone(String phone) {
        VipUser vipUser = vipUserMapper.selectByPrimaryKey(phone);
        VipUserVo vipUserVo = new VipUserVo();
        if (!ObjectUtils.isEmpty(vipUser)) {
            BeanUtils.copyProperties(vipUser, vipUserVo);
            return vipUserVo;
        }
        return null;
    }

    /**
     * 更新用户拥有的积分和积分券
     *
     * @param discountId
     * @param phone
     * @param count
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<UserDiscountVo> updDiscountToUser(int discountId, String phone, int count) {
        //更新用户拥有的积分
        VipUser vipUser = vipUserMapper.selectByPrimaryKey(phone);
        int realPoint = getUserPoint(phone) - pointToDiscount(discountId, count);
        vipUser.setPoint(realPoint);
        vipUserMapper.updateByPrimaryKey(vipUser);

        //查询用户是否拥有此类型优惠券
        UserDiscountExample userDiscountExample = new UserDiscountExample();
        userDiscountExample.createCriteria().andUserPhoneEqualTo(phone).andDiscountIdEqualTo(discountId);
        List<UserDiscount> userDiscountList = userDiscountMapper.selectByExample(userDiscountExample);

        //如果用户未拥有此类型优惠券，直接插入新的记录
        if (ObjectUtils.isEmpty(userDiscountList)) {
            UserDiscount userDiscount = new UserDiscount();
            userDiscount.setCount(count);
            userDiscount.setDiscountId(discountId);
            userDiscount.setUserPhone(phone);
            userDiscountMapper.insertSelective(userDiscount);
            return getUserDiscount(phone);
        }
        //如果用户已拥有此类型优惠券，只增加优惠券的数量
        UserDiscount userDiscountNew = userDiscountList.get(0);
        userDiscountNew.setCount(userDiscountNew.getCount() + count);
        userDiscountMapper.updateByPrimaryKey(userDiscountNew);
        return getUserDiscount(phone);
    }

    /**
     * 根据用户id查询用户拥有的积分券
     *
     * @param phone
     * @return
     */
    @Override
    public List<UserDiscountVo> getUserDiscount(String phone) {
        UserDiscountExample userDiscountExample = new UserDiscountExample();
        userDiscountExample.createCriteria().andUserPhoneEqualTo(phone);
        List<UserDiscount> userDiscountList = userDiscountMapper.selectByExample(userDiscountExample);
        if (!ObjectUtils.isEmpty(userDiscountList)) {
            List<UserDiscountVo> userDiscountVoList = new ArrayList<>();
            for (UserDiscount userDiscount : userDiscountList) {
                UserDiscountVo userDiscountVo = new UserDiscountVo();
                BeanUtils.copyProperties(userDiscount, userDiscountVo);
                userDiscountVoList.add(userDiscountVo);
            }
            return userDiscountVoList;
        }
        return null;
    }

    /**
     * 判断用户现有积分是否足够兑换积分券
     *
     * @param phone
     * @param discountId
     * @param count
     * @return
     */
    @Override
    public Boolean isEnough(String phone, int discountId, int count) {
        //得到用户现有积分
        int nowPoint = getUserPoint(phone);

        //积分券所需要的积分
        int needPoint = pointToDiscount(discountId, count);
        return nowPoint > needPoint;
    }

    /**
     * 查询兑换一定数量的某类型积分券所需的积分
     *
     * @param discountId
     * @param count
     * @return
     */
    @Override
    public int pointToDiscount(int discountId, int count) {
        return discountMapper.selectByPrimaryKey(discountId).getPoint() * count;
    }

    /**
     * 查询用户的会员级别
     *
     * @param phone
     * @return
     */
    @Override
    public Map<String, Object> getUserLevel(String phone) {
        //存储会员信息
        Map<String, Object> memMap = new HashMap();

        //通过登录的用户账号获取用户的最新积分
        int point = getUserPoint(phone);
        String level;
        memMap.put("会员积分", point);

        //根据积分判断会员等级
        if (point >= 50000) {
            level = "白金会员";
        } else if (point >= 10000) {
            level = "金卡会员";
        } else {
            level = "银卡会员";
        }
        memMap.put("会员等级", level);
        return memMap;
    }

    /**
     * 分页查询用户所拥有的积分券
     *
     * @param phone
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageUtils getDiscountList(String phone, int pageNo, int pageSize) {
        List<Map<String, Object>> list = new ArrayList<>();
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        List<UserDiscountVo> userDiscountVoList = getUserDiscount(phone);
        if (ObjectUtils.isEmpty(userDiscountVoList)) {
            return null;
        }
        pageUtils.setTotalCount(userDiscountVoList.size());
        if (pageNo <= 0) {
            pageNo = 1;
        } else if (pageNo > pageUtils.getTotalPage()) {
            pageNo = (int) pageUtils.getTotalPage();
        }

        //分页查询用户拥有的优惠券
        pageUtils.setPageNo(pageNo);
        UserDiscountExample userDiscountExample = new UserDiscountExample();
        userDiscountExample.setLimit(pageUtils.getPageNo());
        userDiscountExample.setOffset(pageSize);
        userDiscountExample.createCriteria().andUserPhoneEqualTo(phone);
        List<UserDiscount> discountList = userDiscountMapper.selectByExample(userDiscountExample);
        if (!ObjectUtils.isEmpty(discountList)) {
            for (UserDiscount userDiscount : discountList) {
                Map<String, Object> discountMap = new HashMap<>();
                //根据积分券id查询积分券详细信息
                Discount discount = discountMapper.selectByPrimaryKey(userDiscount.getDiscountId());
                discountMap.put("面值", discount.getValue() + "元");
                discountMap.put("使用条件", "满" + discount.getCondition() + "元");
                discountMap.put("拥有", userDiscount.getCount() + "张");
                list.add(discountMap);
            }
            pageUtils.setCurrentList(list);
            return pageUtils;
        }
        return null;
    }

    //-------------------游君-----------------------
    //展示用户信息
    @Override
    public VipUserVo showUserInfo(VipUserVo userVo) {
        VipUser vipUser = vipUserMapper.selectByPrimaryKey(userVo.getPhone());
        vipUser.setDataIntegrity(dataIntegrityUtils.countIntegrity(vipUser));
        VipUserVo vipUserVo = new VipUserVo();
        BeanUtils.copyProperties(vipUser, vipUserVo);
        return vipUserVo;
    }

    //修改用户信息
    @Override
    public boolean updateUser(VipUserVo userVo, VipUserVo userNow, String token) {
        //不可更改的信息，用原用户的信息。即使前台更改了这些信息，后台会做修改
        userNow.setPhone(userVo.getPhone());
        userNow.setPoint(userVo.getPoint());
        userNow.setOpenid(userVo.getOpenid());
        userNow.setConSignTimes(userVo.getConSignTimes());
        userNow.setPassword(userVo.getPassword());
        VipUser user = new VipUser();
        BeanUtils.copyProperties(userNow, user);
        Integer count = vipUserMapper.updateByPrimaryKeySelective(user);
        if (count > 0) {
            user = vipUserMapper.selectByPrimaryKey(userVo.getPhone());
            redisUtils.set(ConstantUtils.USER + token, JSONObject.toJSONString(user));
            return true;
        }
        return false;
    }

}
