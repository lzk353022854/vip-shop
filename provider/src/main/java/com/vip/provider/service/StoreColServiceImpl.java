package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.StoreColService;
import com.vip.common.vo.StoresVo;
import com.vip.provider.dto.StoreCollection;
import com.vip.provider.dto.StoreCollectionExample;
import com.vip.provider.dto.Stores;
import com.vip.provider.mapper.StoreCollectionMapper;
import com.vip.provider.mapper.StoresMapper;
import com.vip.provider.util.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StoreColServiceImpl implements StoreColService {

    @Autowired
    private StoresMapper storesMapper;

    @Autowired
    private StoreCollectionMapper storeCollectionMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public List<StoresVo> queryStoreColList(String userPhone, int pageNo, int pageSize) {
        StoreCollectionExample storeCollectionExample = new StoreCollectionExample();
        storeCollectionExample.setLimit(pageNo);
        storeCollectionExample.setOffset(pageSize);
        storeCollectionExample.createCriteria().andUserPhoneEqualTo(userPhone);
        storeCollectionExample.setOrderByClause("create_time DESC");
        List<StoreCollection> storeCollectionList = storeCollectionMapper.selectByExample(storeCollectionExample);
        List<Stores> storeList = Lists.newArrayList();
        storeCollectionList.forEach(storeCollection -> {
            Stores stores = storesMapper.selectByPrimaryKey(storeCollection.getStoreId());
            storeList.add(stores);
        });
        List<StoresVo> storesVoList = Lists.newArrayList();
        storeList.forEach(stores -> {
            StoresVo storesVo = new StoresVo();
            BeanUtils.copyProperties(stores, storesVo);
            storesVoList.add(storesVo);
        });
        return storesVoList;
    }

    @Transactional
    @Override
    public boolean delStoreCol(String userPhone, String storeId) {
        StoreCollectionExample storeCollectionExample = new StoreCollectionExample();
        storeCollectionExample.createCriteria().andUserPhoneEqualTo(userPhone).andStoreIdEqualTo(storeId);
        List<StoreCollection> storeCollectionList = storeCollectionMapper.selectByExample(storeCollectionExample);
        if (storeCollectionList.size() == 1) {
            storeCollectionMapper.deleteByPrimaryKey(storeCollectionList.get(0).getId());
            Stores stores = storesMapper.selectByPrimaryKey(storeId);
            int colNum = stores.getCollectCount() - 1;
            stores.setCollectCount(colNum);
            storesMapper.updateByPrimaryKeySelective(stores);
            redisUtils.del(storeId + ":" + userPhone);
            redisUtils.decr(FinalBean.COLSTORENUM + storeId, 1);
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public String changeStoreCol(String userPhone, String storeId, boolean isCol) {
        try {
            StoreCollectionExample storeCollectionExample = new StoreCollectionExample();
            storeCollectionExample.createCriteria().andUserPhoneEqualTo(userPhone).andStoreIdEqualTo(storeId);
            List<StoreCollection> storeCollectionList = storeCollectionMapper.selectByExample(storeCollectionExample);
            if (storeCollectionList.size() == 1 && !isCol) {
                storeCollectionMapper.deleteByPrimaryKey(storeCollectionList.get(0).getId());
                Stores stores = storesMapper.selectByPrimaryKey(storeId);
                int colNum = stores.getCollectCount() - 1;
                stores.setCollectCount(colNum);
                storesMapper.updateByPrimaryKeySelective(stores);
            } else if (storeCollectionList.size() == 0 && isCol) {
                StoreCollection storeCollection = new StoreCollection();
                storeCollection.setUserPhone(userPhone);
                storeCollection.setStoreId(storeId);
                storeCollectionMapper.insertSelective(storeCollection);
                Stores stores = storesMapper.selectByPrimaryKey(storeId);
                int colNum = stores.getCollectCount() + 1;
                stores.setCollectCount(colNum);
                storesMapper.updateByPrimaryKeySelective(stores);
            }
            return "数据库更新成功";
        } catch (Exception e) {
            return "数据库更新失败";
        }
    }

    @Override
    public int countStoreColNum(String userPhone) {
        StoreCollectionExample storeCollectionExample = new StoreCollectionExample();
        storeCollectionExample.createCriteria().andUserPhoneEqualTo(userPhone);
        return storeCollectionMapper.selectByExample(storeCollectionExample).size();
    }


}
