package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.vip.common.service.BuyService;
import com.vip.common.vo.GoodsVo;
import com.vip.provider.dto.Goods;
import com.vip.provider.dto.GoodsExample;
import com.vip.provider.mapper.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BuyServiceImpl implements BuyService {

    @Autowired
    GoodsMapper goodsMapper;

    public boolean checkNum(GoodsVo goodsVo) {
        GoodsExample goodsExample = new GoodsExample();
        goodsExample.createCriteria().andIdEqualTo(goodsVo.getId());
        List<Goods> goodsList = goodsMapper.selectByExample(goodsExample);
        Goods goods = goodsList.get(0);
        Integer num = goods.getQuantity();
        //库存大于0
        if (num >= 1) {
            return true;
        }
        return false;
    }

}
