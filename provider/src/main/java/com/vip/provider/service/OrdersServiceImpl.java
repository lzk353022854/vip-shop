package com.vip.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.excel.event.Order;
import com.alibaba.fastjson.JSONObject;
import com.vip.common.bean.FinalBean;
import com.vip.common.service.OrdersService;
import com.vip.common.util.CommonUtils;
import com.vip.common.util.PageUtils;
import com.vip.common.vo.OrdersPayVo;
import com.vip.common.vo.OrdersVo;
import com.vip.common.vo.ShopCarVo;
import com.vip.common.vo.VipUserVo;
import com.vip.provider.dto.*;
import com.vip.provider.mapper.*;
import com.vip.provider.util.RedisUtils;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
@Transactional
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private DiscountMapper discountMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private UserDiscountMapper userDiscountMapper;

    @Autowired
    private OrdersPayMapper ordersPayMapper;

    //--------------------------------李誉---------------------------------
    //生成订单
    @Override
    //折扣券如果用户没有，前端应作判断。如果用户没有使用优惠券，或者用户想要使用优惠券但是用户没有拥有该优惠券，则传0
    public OrdersPayVo insertOrder(VipUserVo vipUserVo, List<ShopCarVo> shopCarVoList, int discountId) {
        Set<String> StoresIdSet = new HashSet();
        for (ShopCarVo shopCarVo : shopCarVoList) {
            StoresIdSet.add(shopCarVo.getStoreId());
        }
        List<Orders> ordersList = new ArrayList<Orders>();
        for (String storeId : StoresIdSet) {
            StringBuffer sbGoods = new StringBuffer();
            StringBuffer sbCounts = new StringBuffer();
            double sPay = 0;
            Orders orders = new Orders();
            //生成订单编号
            orders.setId(CommonUtils.getOrderIdOrShopCarId());
            //设置订单店铺名
            orders.setStoreId(storeId);
            //将同一家店铺的商品的商品编号、数量做字符串拼接，并且计算总价
            for (ShopCarVo shopCarVo : shopCarVoList) {
                if (shopCarVo.getStoreId().equals(storeId)) {
                    sbGoods.append(shopCarVo.getGoodsId() + ",");
                    sbCounts.append(shopCarVo.getCount() + ",");
                    Goods goods = goodsMapper.selectByPrimaryKey(shopCarVo.getGoodsId());
                    sPay += goods.getPrice() * shopCarVo.getCount();
                }
            }
            String sbG = sbGoods.substring(0, sbGoods.length() - 1);
            String sbC = sbCounts.substring(0, sbCounts.length() - 1);
            //设置商品编号
            orders.setGoodsId(sbG);
            //设置手机号
            orders.setPhone(vipUserVo.getPhone());
            //设置商品数量
            orders.setCount(sbC);
            //设置应付价格
            orders.setsPay(sPay);
            //存入订单列表，此时未设置实付价格
            ordersList.add(orders);
        }
        //所有订单应付金额
        double allOrdersSPay = 0;
        for (int i = 0; i < ordersList.size(); i++) {
            allOrdersSPay += ordersList.get(i).getsPay();
        }
        //所有订单实付金额
        double allOrdersAPay = 0;
        //查询折扣券对象
        Discount discount = discountMapper.selectByPrimaryKey(discountId);
        //折扣券，0为不使用
        if (0 != discountId) {
            UserDiscountExample userDiscountExample = new UserDiscountExample();
            userDiscountExample.createCriteria().andUserPhoneEqualTo(vipUserVo.getPhone()).andDiscountIdEqualTo(discountId);
            List<UserDiscount> userDiscounts = userDiscountMapper.selectByExample(userDiscountExample);
            //判断用户是否拥有该折扣券
            if (userDiscounts.size() > 0 && userDiscounts.get(0).getCount() > 0) {
                //满足使用条件
                if (discount.getCondition() <= allOrdersSPay) {
                    allOrdersAPay = allOrdersSPay - discount.getValue();
                } else {
                    //不满足折扣券使用条件，实付金额为应付金额
                    allOrdersAPay = allOrdersSPay;
                }
            } else {
                //用户想用却没有该折扣券，实付金额为应付金额
                allOrdersAPay = allOrdersSPay;
            }
        } else {
            //不使用折扣券，实付金额为应付金额
            allOrdersAPay = allOrdersSPay;
        }
        List<OrdersVo> ordersVoList = new ArrayList<OrdersVo>();
        StringBuffer sbIdAll = new StringBuffer();
        //设置每个订单的应付金额，为该订单实付金额减去该订单实付金额占所有订单实付总金额的百分比乘以折扣金额
        for (int i = 0; i < ordersList.size(); i++) {
            Orders orders = ordersList.get(i);
            //修改每个订单的金额
            if (0 != discountId) {
                UserDiscountExample userDiscountExample = new UserDiscountExample();
                userDiscountExample.createCriteria().andUserPhoneEqualTo(vipUserVo.getPhone()).andDiscountIdEqualTo(discountId);
                List<UserDiscount> userDiscounts = userDiscountMapper.selectByExample(userDiscountExample);
                //判断用户是否拥有该折扣券
                if (userDiscounts.size() > 0 && userDiscounts.get(0).getCount() > 0) {
                    //满足使用条件，修改每个订单的应付金额
                    if (discount.getCondition() <= allOrdersSPay) {
                        orders.setaPay(orders.getsPay() - (orders.getsPay() / allOrdersSPay) * discount.getValue());
                    } else {
                        //不满足折扣券使用条件，每个订单实付金额为应付金额
                        orders.setaPay(orders.getsPay());
                    }
                } else {
                    //用户想用却没有该折扣券，每个订单实付金额为应付金额
                    orders.setaPay(orders.getsPay());
                }
            } else {
                //不使用折扣券，每个订单实付金额为应付金额
                orders.setaPay(orders.getsPay());
            }
            //循环订单列表，一个一个存Redis
            String orderStr = JSONObject.toJSONString(orders);
            redisUtils.set(orders.getId(), orderStr);
            //循环订单列表，一个一个存MySql
            ordersMapper.insertSelective(orders);
            sbIdAll.append(orders.getId() + "-");
            OrdersVo ordersVo = new OrdersVo();
            BeanUtils.copyProperties(orders, ordersVo);
            ordersVoList.add(ordersVo);
        }
        String sbAll = sbIdAll.substring(0, sbIdAll.length() - 1);
        OrdersPayVo ordersPayVo = new OrdersPayVo();
        ordersPayVo.setaPay(allOrdersAPay);
        ordersPayVo.setOrdersId(sbAll);
        ordersPayVo.setId(CommonUtils.getOrderIdOrShopCarId());
        ordersPayVo.setPhone(vipUserVo.getPhone());
        ordersPayVo.setDiscountId(discountId);
        OrdersPay ordersPay = new OrdersPay();
        BeanUtils.copyProperties(ordersPayVo, ordersPay);
        ordersPayMapper.insertSelective(ordersPay);
        return ordersPayVo;
        //下面代码是生成一个订单
//        Orders orders = new Orders();
//        orders.setId(CommonUtils.getOrderIdOrShopCarId());
//        orders.setStoreId(shopCarVoList.get(0).getStoreId());
//        StringBuffer sbGoods = new StringBuffer();
//        StringBuffer sbCounts = new StringBuffer();
//        double sPay = 0;
//        for (ShopCarVo shopCarVo : shopCarVoList) {
//            int quantityNum = goodsMapper.selectByPrimaryKey(shopCarVo.getGoodsId()).getQuantity();
//            if (shopCarVo.getCount() > quantityNum) {
//                return null;
//            }
//            sbGoods.append(shopCarVo.getGoodsId() + ",");
//            sbCounts.append(shopCarVo.getCount() + ",");
//            Goods goods = goodsMapper.selectByPrimaryKey(shopCarVo.getGoodsId());
//            sPay += goods.getPrice() * shopCarVo.getCount();
//        }
//        String sbG = sbGoods.substring(0, sbGoods.length() - 1);
//        String sbC = sbCounts.substring(0, sbCounts.length() - 1);
//        orders.setGoodsId(sbG);
//        orders.setPhone(vipUserVo.getPhone());
//        orders.setCount(sbC);
//        orders.setsPay(sPay);
//        if (0 != discountId) {
//            UserDiscountExample userDiscountExample = new UserDiscountExample();
//            userDiscountExample.createCriteria().andUserPhoneEqualTo(vipUserVo.getPhone()).andDiscountIdEqualTo(discountId);
//            List<UserDiscount> userDiscounts = userDiscountMapper.selectByExample(userDiscountExample);
//            if (userDiscounts.size() > 0 && userDiscounts.get(0).getCount() > 0) {
//                Discount discount = discountMapper.selectByPrimaryKey(discountId);
//                if (discount.getCondition() > sPay) {
//                    orders.setaPay(sPay);
//                } else {
//                    orders.setaPay(sPay - discount.getValue());
//                }
//            }
//        } else {
//            orders.setaPay(sPay);
//        }
//        //将订单存入Redis
//        redisUtils.set(orders.getId(), JSONObject.toJSONString(orders));
//        ordersMapper.insertSelective(orders);
//        OrdersVo ordersVo = new OrdersVo();
//        BeanUtils.copyProperties(orders, ordersVo);
//        return ordersVo;
    }

    //支付失败，该用户用的该优惠券复原
    public void payFailAddDiscountCount(String orderPayId) {
        OrdersPay ordersPay = ordersPayMapper.selectByPrimaryKey(orderPayId);
        UserDiscount userDiscount = new UserDiscount();
        UserDiscountExample userDiscountExample = new UserDiscountExample();
        userDiscountExample.createCriteria().andUserPhoneEqualTo(ordersPay.getPhone()).andDiscountIdEqualTo(ordersPay.getDiscountId());
        List<UserDiscount> userDiscountList = userDiscountMapper.selectByExample(userDiscountExample);
        userDiscount.setCount(userDiscountList.get(0).getCount() + 1);
        userDiscountMapper.updateByExampleSelective(userDiscount, userDiscountExample);
    }


    //生成订单该该用户所拥有该种优惠券库存
    public void updateDiscountOfVipUser(VipUserVo vipUserVo, int discountId, double allOrdersSPay) {
        if (0 != discountId) {
            Discount discount = discountMapper.selectByPrimaryKey(discountId);
            UserDiscountExample userDiscountExample = new UserDiscountExample();
            userDiscountExample.createCriteria().andUserPhoneEqualTo(vipUserVo.getPhone()).andDiscountIdEqualTo(discountId);
            List<UserDiscount> userDiscounts = userDiscountMapper.selectByExample(userDiscountExample);
            //判断用户是否拥有该折扣券
            if (userDiscounts.size() > 0 && userDiscounts.get(0).getCount() > 0) {
                //满足使用条件，该用户用的此折扣券类型数量减一
                if (discount.getCondition() <= allOrdersSPay) {
                    UserDiscount userDiscount = new UserDiscount();
                    userDiscountExample.createCriteria().andUserPhoneEqualTo(vipUserVo.getPhone()).andDiscountIdEqualTo(discountId);
                    userDiscount.setCount(userDiscounts.get(0).getCount() - 1);
                    userDiscountMapper.updateByExampleSelective(userDiscount, userDiscountExample);
                }
            }
        }
    }


    //根据orderId查order并转换成orderVo
    @Override
    public OrdersVo checkOrder(String orderId) {
        Orders orders = ordersMapper.selectByPrimaryKey(orderId);
        OrdersVo ordersVo = new OrdersVo();
        BeanUtils.copyProperties(orders, ordersVo);
        return ordersVo;
    }

    //根据orderPayId查orderPay并转换成orderPayVo
    @Override
    public OrdersPayVo checkOrderPay(String orderPayId) {
        OrdersPay ordersPay = ordersPayMapper.selectByPrimaryKey(orderPayId);
        OrdersPayVo ordersPayVo = new OrdersPayVo();
        BeanUtils.copyProperties(ordersPay, ordersPayVo);
        return ordersPayVo;
    }

    //订单取消，库存回滚
    @Override
    public boolean payFailAddNum(OrdersVo ordersVo) {
        String[] goodsIdString = ordersVo.getGoodsId().split(",");
        List<String> goodsIdsList = Arrays.asList(goodsIdString);
        String[] countsString = ordersVo.getCount().split(",");
        List<String> countsList = Arrays.asList(countsString);
        for (int i = 0; i < goodsIdsList.size(); i++) {
            Goods goods = goodsMapper.selectByPrimaryKey(goodsIdsList.get(i));
            // todo
            goods.setQuantity(goods.getQuantity() + Integer.valueOf(countsList.get(i)));
            goodsMapper.updateByPrimaryKeySelective(goods);
        }
        return true;
    }

    //改订单状态为已支付以及支付时间
    @Override
    public void changeOrderStateAndPayTime(OrdersVo ordersVo) {
        ordersVo.setState(1);
        Orders orders = new Orders();
        BeanUtils.copyProperties(ordersVo, orders);
        ordersMapper.updateByPrimaryKeySelective(orders);
    }


    //订单失效，改变state状态
    public void OrdersCancelChangeState(OrdersVo ordersVo) {
        //4为已取消
        ordersVo.setState(4);
        Orders orders = new Orders();
        BeanUtils.copyProperties(ordersVo, orders);
        ordersMapper.updateByPrimaryKeySelective(orders);
    }


    //------------------------陈佩琳------------------------------
    @Override
    public PageUtils<List<OrdersVo>> selectOrders(String userId, Integer state, int pageNo) {
        OrdersExample ordersExample = new OrdersExample();
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setPageSize(FinalBean.SEARCHSIZE);
        pageUtils.setCurrentPage(pageNo);
        ordersExample.setOrderByClause("`create_time`DESC");
        ordersExample.setLimit(pageUtils.getPageNo());
        ordersExample.setOffset(pageUtils.getPageSize());
        if (null != state && null != userId) {
            ordersExample.createCriteria().andPhoneEqualTo(userId).andStateEqualTo(state);
        } else if (null != state && null == userId) {
            ordersExample.createCriteria().andStateEqualTo(state);
        } else if (null == state && null != userId) {
            ordersExample.createCriteria().andPhoneEqualTo(userId);
        }
        List<Orders> ordersList = ordersMapper.selectByExample(ordersExample);
        List<OrdersVo> ordersVoList = new ArrayList<OrdersVo>();
        ordersList.forEach(orders -> {
            OrdersVo ordersVo = new OrdersVo();
            BeanUtils.copyProperties(orders, ordersVo);
            ordersVoList.add(ordersVo);
        });
        pageUtils.setCurrentList(ordersVoList);
        long totalCount = ordersMapper.countByExample(ordersExample);
        pageUtils.setTotalCount(totalCount);
        return pageUtils;
    }

}
