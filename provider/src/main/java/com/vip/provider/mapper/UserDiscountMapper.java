package com.vip.provider.mapper;

import com.vip.provider.dto.UserDiscount;
import com.vip.provider.dto.UserDiscountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserDiscountMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    long countByExample(UserDiscountExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int deleteByExample(UserDiscountExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int insert(UserDiscount record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int insertSelective(UserDiscount record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    List<UserDiscount> selectByExample(UserDiscountExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    UserDiscount selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") UserDiscount record, @Param("example") UserDiscountExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") UserDiscount record, @Param("example") UserDiscountExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(UserDiscount record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_discount
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(UserDiscount record);
}