package com.vip.provider.mapper;

import com.vip.provider.dto.OrdersPay;
import com.vip.provider.dto.OrdersPayExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrdersPayMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    long countByExample(OrdersPayExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int deleteByExample(OrdersPayExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int insert(OrdersPay record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int insertSelective(OrdersPay record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    List<OrdersPay> selectByExample(OrdersPayExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    OrdersPay selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") OrdersPay record, @Param("example") OrdersPayExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") OrdersPay record, @Param("example") OrdersPayExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(OrdersPay record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table orders_pay
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(OrdersPay record);
}