package com.vip.provider.mapper;

import com.vip.provider.dto.StoreCollection;
import com.vip.provider.dto.StoreCollectionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface StoreCollectionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    long countByExample(StoreCollectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int deleteByExample(StoreCollectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int insert(StoreCollection record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int insertSelective(StoreCollection record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    List<StoreCollection> selectByExample(StoreCollectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    StoreCollection selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") StoreCollection record, @Param("example") StoreCollectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") StoreCollection record, @Param("example") StoreCollectionExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(StoreCollection record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store_collection
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(StoreCollection record);
}