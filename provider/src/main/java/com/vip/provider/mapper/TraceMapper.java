package com.vip.provider.mapper;

import com.vip.provider.dto.Trace;
import com.vip.provider.dto.TraceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TraceMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    long countByExample(TraceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int deleteByExample(TraceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int insert(Trace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int insertSelective(Trace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    List<Trace> selectByExample(TraceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    Trace selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Trace record, @Param("example") TraceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Trace record, @Param("example") TraceExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Trace record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table trace
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Trace record);
}