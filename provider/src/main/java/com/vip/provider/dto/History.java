package com.vip.provider.dto;

import java.util.Date;

/**
 * Database Table Remarks:
 *   搜索历史表
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table history
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class History {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column history.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column history.user_phone
     *
     * @mbg.generated
     */
    private String userPhone;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column history.content
     *
     * @mbg.generated
     */
    private String content;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column history.search_time
     *
     * @mbg.generated
     */
    private Date searchTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column history.id
     *
     * @return the value of history.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column history.id
     *
     * @param id the value for history.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column history.user_phone
     *
     * @return the value of history.user_phone
     *
     * @mbg.generated
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column history.user_phone
     *
     * @param userPhone the value for history.user_phone
     *
     * @mbg.generated
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column history.content
     *
     * @return the value of history.content
     *
     * @mbg.generated
     */
    public String getContent() {
        return content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column history.content
     *
     * @param content the value for history.content
     *
     * @mbg.generated
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column history.search_time
     *
     * @return the value of history.search_time
     *
     * @mbg.generated
     */
    public Date getSearchTime() {
        return searchTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column history.search_time
     *
     * @param searchTime the value for history.search_time
     *
     * @mbg.generated
     */
    public void setSearchTime(Date searchTime) {
        this.searchTime = searchTime;
    }
}