package com.vip.provider.dto;

import java.util.Date;

/**
 * Database Table Remarks:
 *   店铺收藏表
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table store_collection
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class StoreCollection {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column store_collection.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column store_collection.user_phone
     *
     * @mbg.generated
     */
    private String userPhone;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column store_collection.store_id
     *
     * @mbg.generated
     */
    private String storeId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column store_collection.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column store_collection.id
     *
     * @return the value of store_collection.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column store_collection.id
     *
     * @param id the value for store_collection.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column store_collection.user_phone
     *
     * @return the value of store_collection.user_phone
     *
     * @mbg.generated
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column store_collection.user_phone
     *
     * @param userPhone the value for store_collection.user_phone
     *
     * @mbg.generated
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column store_collection.store_id
     *
     * @return the value of store_collection.store_id
     *
     * @mbg.generated
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column store_collection.store_id
     *
     * @param storeId the value for store_collection.store_id
     *
     * @mbg.generated
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column store_collection.create_time
     *
     * @return the value of store_collection.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column store_collection.create_time
     *
     * @param createTime the value for store_collection.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}