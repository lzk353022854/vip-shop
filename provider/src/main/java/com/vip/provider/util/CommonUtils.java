package com.vip.provider.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CommonUtils {

    public static String getUUID() {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
        return uuid;
    }

}
