package com.vip.provider.util;

import com.vip.common.vo.VipUserVo;
import com.vip.provider.dto.VipUser;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@Component
public class DataIntegrityUtils {
    public String countIntegrity(VipUser vipUser) {
        NumberFormat format = new DecimalFormat("0.0");
        double count = 0;
        if (!StringUtils.isEmpty(vipUser.getIntroduce())) {
            count = count + 0.2;
        }
        if (vipUser.getSex() != null) {
            count = count + 0.2;
        }

        if (!StringUtils.isEmpty(vipUser.getHead())) {
            count = count + 0.2;
        }

        if (vipUser.getBirthday() != null) {
            count = count + 0.2;
        }

        if (!StringUtils.isEmpty(vipUser.getEmail())) {
            count = count + 0.2;
        }

        String integrity = format.format(count * 100) + "%";
        return integrity;
    }
}
