package com.vip.provider.util;

import lombok.Data;

import java.io.Serializable;


@Data
public class ReturnResult<T> implements Serializable {

    private static final long serialVersionUID = 6630193577918253203L;
    private Integer code;
    private String message;
    private T data;

}
