package com.vip.provider.config;

/**
 * Created by xueqijun on 2020/5/28.
 */
public class MultipleDataSourceHelper {
    public static final String MASTER = "master";

    public static final String SALVE = "salve";

    private static ThreadLocal<String> contextHolder = new ThreadLocal<String>();

    public static void set(String db) {
        contextHolder.set(db);
    }

    public static String get() {
        return contextHolder.get();
    }
}
