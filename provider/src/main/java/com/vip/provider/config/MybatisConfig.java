package com.vip.provider.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(value = "com.vip.provider.mapper")
public class MybatisConfig {

}
