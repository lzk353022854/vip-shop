package com.vip.provider.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * Created by xueqijun on 2020/5/28.
 */
@Component
@Aspect
public class DSSelectorImpl {
    @Before("com.vip.provider.config.DSPointcut.selectDsPonitcut()")
    public void chageDs(JoinPoint joinPoint) throws NoSuchMethodException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Method realMethod = joinPoint.getTarget().getClass().getDeclaredMethod(signature.getName(), method.getParameterTypes());
        DSSelector dataSourceSelector = realMethod.getAnnotation(DSSelector.class);
        if (null == dataSourceSelector) return;

        MultipleDataSourceHelper.set(dataSourceSelector.value());
    }
}
