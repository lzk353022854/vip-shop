package com.vip.common.service;

import com.vip.common.util.PageUtils;
import com.vip.common.vo.UserDiscountVo;
import com.vip.common.vo.VipUserVo;

import java.util.List;
import java.util.Map;

public interface VipUserService {

    //--------------------游君--------------------
    // 展示用户基本信息
    VipUserVo showUserInfo(VipUserVo userVo);

    // 修改用户信息
    boolean updateUser(VipUserVo userVo, VipUserVo userNow, String userKey);

    //--------------------黄一帆--------------------
    //查询用户积分
    int getUserPoint(String phone);

    //用户注册
    void insertUser(String phone, String passward) throws Exception;

    //查询用户是否存在
    boolean isExistsUser(String phone, String passward) throws Exception;

    //查询微信是否绑定手机号
    boolean isBind(String openid);

    //绑定手机号
    void bindUser(VipUserVo vipUserVo);

    //根据openid查询用户
    VipUserVo queryUserByOpenid(String openid);

    //根据手机号查询用户
    VipUserVo queryByPhone(String phone);

    //更新用户拥有的积分和积分券
    List<UserDiscountVo> updDiscountToUser(int discountId, String phone, int count);

    //查询用户拥有的积分券
    List<UserDiscountVo> getUserDiscount(String phone);

    //判断用户拥有积分是否足够兑换优惠券
    Boolean isEnough(String phone, int discountId, int count);

    //查询积分券对应的积分
    int pointToDiscount(int discountId, int count);

    //判断用户会员等级
    Map<String, Object> getUserLevel(String phone);

    //展示用户拥有的积分券
    PageUtils getDiscountList(String phone, int pageNo, int pageSize);


    //---------------------李誉
    VipUserVo checkVipUser(String phone);

    void changeVipUserPoint(VipUserVo vipUserVo);

    //----------------------陈佩琳
    boolean updateSignIn(String UserId, int num);

    List<VipUserVo> selectVipUserAll();

    boolean updatePoint(String userId, int point);


}
