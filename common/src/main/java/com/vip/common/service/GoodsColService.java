package com.vip.common.service;

import com.vip.common.vo.GoodsCollectionVo;
import com.vip.common.vo.GoodsVo;

import java.util.List;

public interface GoodsColService {

    //查询用户收藏的商品
    List<GoodsVo> goodsColByUser(int pageNo, int pageSize, String phone);

    //查询用户收藏的商品数量
    int goodsColNum(String phone);
}
