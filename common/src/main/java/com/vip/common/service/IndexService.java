package com.vip.common.service;

import com.vip.common.vo.TagVo;

import java.util.List;

public interface IndexService {

    List<TagVo> showIndex(Integer tagId);

}
