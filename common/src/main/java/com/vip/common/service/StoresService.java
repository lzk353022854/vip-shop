package com.vip.common.service;

import com.vip.common.vo.StoresVo;

public interface StoresService {
    //查询店铺
    StoresVo storeCollection(String storeId);

}
