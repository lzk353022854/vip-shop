package com.vip.common.service;

import com.vip.common.util.PageUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.ShopCarVo;

import java.util.List;

public interface GoodsService {

    //---------------------黄一帆---------------------------
    //批量增加商品
    void insertGoods(List<GoodsVo> goodsVoList);

    //商品上架
    void updGoodsStateOn(List<String> goodsIdList);

    //商品下架
    void updGoodsStateOff(List<String> goodsIdList);

    //更新商品点赞数
    void updGoodsLike(String goodsId, boolean flag);

    //更新商品收藏数
    void updGoodsCollect(String goodsId, boolean flag);

    //更新redis及mysql中用户对商品的收藏记录、商品的收藏量
    void updGoodsCollectMore(String goodsId, String phone, String action);

    //更新redis及mysql中用户对商品的点赞记录、商品的点赞量
    void updGoodsLikeMore(String goodsId, String phone, String action);

    //判断用户的点赞/收藏状态
    int isOkAction(boolean flag, String goodsId, String phone, String action);

    //分页查询用户收藏的商品
    List<GoodsVo> goodsColByUser(int pageNo, int pageSize, String phone);

    //分页查询用户点赞的商品
    List<GoodsVo> goodsLikeByUser(int pageNo, int pageSize, String phone);

    //查询用户收藏商品的总条数
    int goodsColNum(String phone);

    //查询用户点赞商品的总条数
    int goodsLikeNum(String phone);

    //对分页查询的用户收藏的商品结果封装
    PageUtils goodsColByUserByPage(int pageNo, int pageSize, String phone, int totalCount);

    //对分页查询的用户点赞的商品结果封装
    PageUtils goodsLikeByUserByPage(int pageNo, int pageSize, String phone, int totalCount);

    //分页
    PageUtils getPageUtils(int pageNo, int pageSize, int totalCount);

    //修改商品信息
    void updGoodsDetail(GoodsVo goodsVo);

    //-------------------李誉
    void changeQuantity(List<ShopCarVo> shopCarVoList);

    //-------------------陈佩琳

    PageUtils selectGoods(String name, int choose, int pageNo, int pageSize);

    PageUtils RecommendselectGoods(String userId, int pageNo, int pageSize);


}
