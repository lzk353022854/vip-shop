package com.vip.common.service;

import com.vip.common.util.PageUtils;
import com.vip.common.vo.StoresVo;

import java.util.List;

public interface StoreService {

    //-------------------------------黄一帆---------------------------------------
    boolean addStore(StoresVo storesVo);

    List<StoresVo> showStore(int pageNo, int pageSize);

    long countStore();

    boolean freezeStore(String storeId);

    boolean thawStore(String storeId);

    boolean modStore(StoresVo storesVo);

    //--------------------------------陈佩琳-------------------------------------------
    PageUtils<List<StoresVo>> selectStoreVo(String name, int choose, int pageNo, int pageSize);


}
