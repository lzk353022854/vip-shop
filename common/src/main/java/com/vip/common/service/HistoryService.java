package com.vip.common.service;

import com.vip.common.vo.HisToryVo;

import java.util.List;

/**
 * Created by Administrator on 2020/6/3.
 */
public interface HistoryService {

    boolean updateHistory(HisToryVo hisToryVo);

    boolean deleteHistory(String userId, Integer historyId);

    boolean addHistory(HisToryVo hisToryVo);

    List<HisToryVo> selectHistoryByTen(String userId, String name);

    HisToryVo isExistsByContextAndUserId(String userId, String context);

    boolean searchAdd(String userId, String context);

    boolean deleteHistoryNotInTen(String userId);
}
