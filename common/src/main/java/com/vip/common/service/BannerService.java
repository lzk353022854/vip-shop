package com.vip.common.service;

import com.vip.common.vo.BannerVo;

import java.util.List;

public interface BannerService {
    //----------------------李振坤----------------------
    //获取首页banner
    List<BannerVo> getBannerList();

}
