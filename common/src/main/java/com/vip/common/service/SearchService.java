package com.vip.common.service;

import com.vip.common.util.PageUtils;
import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.StoresVo;

import java.util.List;

/**
 * Created by Administrator on 2020/6/5.
 */
public interface SearchService {

    PageUtils<List<StoresVo>> searchStores(String name, int choose, int pageNo, String userId) throws Exception;

    PageUtils<List<GoodsVo>> searchGoods(String name, int choose, int pageNo, String userId) throws Exception;

}
