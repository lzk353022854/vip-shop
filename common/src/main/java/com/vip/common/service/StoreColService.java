package com.vip.common.service;

import com.vip.common.vo.StoresVo;
import com.vip.common.vo.VipUserVo;

import java.util.List;

public interface StoreColService {

    List<StoresVo> queryStoreColList(String userPhone, int pageNo, int pageSize);

    boolean delStoreCol(String userPhone, String storeId);

    String changeStoreCol(String userPhone, String storeId, boolean isCol);

    int countStoreColNum(String userPhone);

}
