package com.vip.common.service;

import com.vip.common.vo.TAddressCityVo;
import com.vip.common.vo.TAddressProvinceVo;

import java.util.List;

public interface ProvinceService {

    List<TAddressProvinceVo> getProvince(Integer proId);

    List<TAddressCityVo> getCity(TAddressProvinceVo proVo, Integer cityId);

}
