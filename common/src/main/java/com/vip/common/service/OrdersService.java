package com.vip.common.service;

import com.vip.common.util.PageUtils;
import com.vip.common.vo.*;

import java.util.List;

public interface OrdersService {

    //-------------------------李誉---------------------------
    //生成订单
    OrdersPayVo insertOrder(VipUserVo vipUserVo, List<ShopCarVo> shopCarVoList, int discountId);

    //支付失败折扣券回滚
    void payFailAddDiscountCount(String orderPayId);

    //支付成功更新用户拥有的折扣券
    void updateDiscountOfVipUser(VipUserVo vipUserVo, int discountId, double allOrdersSPay);

    //根据orderId查找order
    OrdersVo checkOrder(String orderId);

    //根据orderPayId查找orderPay
    OrdersPayVo checkOrderPay(String orderPayId);

    //支付失败商品库存回滚
    boolean payFailAddNum(OrdersVo ordersVo);

    //支付成功改订单状态和支付时间
    void changeOrderStateAndPayTime(OrdersVo ordersVo);

    //订单取消
    void OrdersCancelChangeState(OrdersVo ordersVo);


    //-----------------------陈佩琳----------------------
    PageUtils<List<OrdersVo>> selectOrders(String userId, Integer state, int pageNo);

}
