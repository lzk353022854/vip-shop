package com.vip.common.service;

import com.vip.common.vo.ActivitiesVo;
import com.vip.common.vo.TagVo;

import java.util.List;
import java.util.Map;

public interface TagService {

    Map<TagVo, List<ActivitiesVo>> showTags();

    ActivitiesVo addActivity(ActivitiesVo activitiesVo);

}
