package com.vip.common.service;

import com.vip.common.vo.GoodsVo;

import java.util.List;
import java.util.Map;

public interface TraceService {

    Map<String, Object> showGood(String userPhone, String goodId);

    List<GoodsVo> showTraceList(String userPhone, int pageNo, int pageSize);

    int getTraceCount(String userPhone);

    void addTrace(String userPhone, String goodId);
}
