package com.vip.common.service;

import com.vip.common.vo.GoodsLikeVo;
import com.vip.common.vo.GoodsVo;

import java.util.List;

public interface GoodsLikeService {

    //查询用户点赞的商品数量
    int goodsLikeNum(String phone);

    //查询用户点赞的商品
    List<GoodsVo> goodsLikeByUser(int pageNo, int pageSize, String phone);
}
