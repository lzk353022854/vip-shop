package com.vip.common.service;

public interface DiscountService {

    //根据积分券Id获得所需积分
    int getNeedPoint(int discountId);
}
