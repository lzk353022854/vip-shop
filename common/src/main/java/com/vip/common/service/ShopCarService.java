package com.vip.common.service;

import com.vip.common.vo.GoodsVo;
import com.vip.common.vo.ShopCarVo;

import java.util.List;
import java.util.Map;

public interface ShopCarService {

    String addShopCar(String goodId, String phone, int count);

    List<Map<String, Object>> queryShopCar(int pageNo, int pageSize, String phone);

    long getTotalCount(String phone);

    int deleteGoodFromShopCarList(String phone, String... goodsId);

    String changeCountsOfGoodsFromShopCarList(String shopCarId, int counts);

}
