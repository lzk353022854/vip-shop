package com.vip.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Database Table Remarks:
 * 商品收藏表
 * <p>
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table goods_collection
 *
 * @mbg.generated do_not_delete_during_merge
 */
@ApiModel
public class GoodsCollectionVo implements Serializable {
    private static final long serialVersionUID = -6230959340466982757L;
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column goods_collection.id
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "商品收藏id")
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column goods_collection.user_phone
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "手机号")
    private String userPhone;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column goods_collection.goods_id
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "商品编号")
    private String goodsId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column goods_collection.id
     *
     * @return the value of goods_collection.id
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column goods_collection.id
     *
     * @param id the value for goods_collection.id
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column goods_collection.user_phone
     *
     * @return the value of goods_collection.user_phone
     * @mbg.generated
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column goods_collection.user_phone
     *
     * @param userPhone the value for goods_collection.user_phone
     * @mbg.generated
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column goods_collection.goods_id
     *
     * @return the value of goods_collection.goods_id
     * @mbg.generated
     */
    public String getGoodsId() {
        return goodsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column goods_collection.goods_id
     *
     * @param goodsId the value for goods_collection.goods_id
     * @mbg.generated
     */
    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }
}