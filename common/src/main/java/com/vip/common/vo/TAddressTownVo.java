package com.vip.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Database Table Remarks:
 * 区县信息表
 * <p>
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table t_address_town
 *
 * @mbg.generated do_not_delete_during_merge
 */
@ApiModel
public class TAddressTownVo implements Serializable {
    /**
     * Database Column Remarks:
     * 主键
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_address_town.id
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "县id")
    private Integer id;

    /**
     * Database Column Remarks:
     * 区县编码
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_address_town.code
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "县编码")
    private String code;

    /**
     * Database Column Remarks:
     * 区县名称
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_address_town.name
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "县名")
    private String name;

    /**
     * Database Column Remarks:
     * 所属城市编码
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_address_town.cityCode
     *
     * @mbg.generated
     */
    @ApiModelProperty(notes = "所属市编码")
    private String citycode;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_address_town.id
     *
     * @return the value of t_address_town.id
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_address_town.id
     *
     * @param id the value for t_address_town.id
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_address_town.code
     *
     * @return the value of t_address_town.code
     * @mbg.generated
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_address_town.code
     *
     * @param code the value for t_address_town.code
     * @mbg.generated
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_address_town.name
     *
     * @return the value of t_address_town.name
     * @mbg.generated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_address_town.name
     *
     * @param name the value for t_address_town.name
     * @mbg.generated
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_address_town.cityCode
     *
     * @return the value of t_address_town.cityCode
     * @mbg.generated
     */
    public String getCitycode() {
        return citycode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_address_town.cityCode
     *
     * @param citycode the value for t_address_town.cityCode
     * @mbg.generated
     */
    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }
}