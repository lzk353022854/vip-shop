package com.vip.common.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Database Table Remarks:
 * 订单表
 * <p>
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table orders
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class OrdersVo implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.id
     *
     * @mbg.generated
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.store_id
     *
     * @mbg.generated
     */
    private String storeId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.goods_id
     *
     * @mbg.generated
     */
    private String goodsId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.phone
     *
     * @mbg.generated
     */
    private String phone;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.count
     *
     * @mbg.generated
     */
    private String count;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.s_pay
     *
     * @mbg.generated
     */
    private Double sPay;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.discount_id
     *
     * @mbg.generated
     */
    private Integer discountId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.a_pay
     *
     * @mbg.generated
     */
    private Double aPay;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.state
     *
     * @mbg.generated
     */
    private Integer state;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.pay_time
     *
     * @mbg.generated
     */
    private Date payTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.id
     *
     * @return the value of orders.id
     * @mbg.generated
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.id
     *
     * @param id the value for orders.id
     * @mbg.generated
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.store_id
     *
     * @return the value of orders.store_id
     * @mbg.generated
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.store_id
     *
     * @param storeId the value for orders.store_id
     * @mbg.generated
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.goods_id
     *
     * @return the value of orders.goods_id
     * @mbg.generated
     */
    public String getGoodsId() {
        return goodsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.goods_id
     *
     * @param goodsId the value for orders.goods_id
     * @mbg.generated
     */
    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.phone
     *
     * @return the value of orders.phone
     * @mbg.generated
     */
    public String getPhone() {
        return phone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.phone
     *
     * @param phone the value for orders.phone
     * @mbg.generated
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.count
     *
     * @return the value of orders.count
     * @mbg.generated
     */
    public String getCount() {
        return count;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.count
     *
     * @param count the value for orders.count
     * @mbg.generated
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.s_pay
     *
     * @return the value of orders.s_pay
     * @mbg.generated
     */
    public Double getsPay() {
        return sPay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.s_pay
     *
     * @param sPay the value for orders.s_pay
     * @mbg.generated
     */
    public void setsPay(Double sPay) {
        this.sPay = sPay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.discount_id
     *
     * @return the value of orders.discount_id
     * @mbg.generated
     */
    public Integer getDiscountId() {
        return discountId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.discount_id
     *
     * @param discountId the value for orders.discount_id
     * @mbg.generated
     */
    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.a_pay
     *
     * @return the value of orders.a_pay
     * @mbg.generated
     */
    public Double getaPay() {
        return aPay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.a_pay
     *
     * @param aPay the value for orders.a_pay
     * @mbg.generated
     */
    public void setaPay(Double aPay) {
        this.aPay = aPay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.state
     *
     * @return the value of orders.state
     * @mbg.generated
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.state
     *
     * @param state the value for orders.state
     * @mbg.generated
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.create_time
     *
     * @return the value of orders.create_time
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.create_time
     *
     * @param createTime the value for orders.create_time
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.pay_time
     *
     * @return the value of orders.pay_time
     * @mbg.generated
     */
    public Date getPayTime() {
        return payTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.pay_time
     *
     * @param payTime the value for orders.pay_time
     * @mbg.generated
     */
    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }
}