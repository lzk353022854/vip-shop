package com.vip.common.util;

public class ConstantUtils {

    public static final String USER = "user:";

    public static final String LIKENUM = "likeNum:";

    public static final String LIKE = "like";

    public static final String COLLECT = "collect";

    public static final String COLLECTNUM = "collectNum:";

}
