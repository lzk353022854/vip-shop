package com.vip.common.util;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;

@Data
@Component
public class PageUtils<T> implements Serializable {

    private int currentPage;
    private int pageNo;
    private int pageSize = 3;//每页展示条数
    private long totalCount;//总条数
    private long totalPage;//总页数
    private Collection<T> currentList;


    public int getPageNo() {
        return (pageNo - 1) * pageSize;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public long getTotalPage() {
        return totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }
}
