package com.vip.common.util;


import java.util.UUID;


public class CommonUtils {

    //生成订单编号或者购物车编号
    public static String getOrderIdOrShopCarId() {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "").substring(0, 20);
        return uuid;
    }

    //生成商品编号
    public static String getGoodId() {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "").substring(0, 10);
        return uuid;
    }

    //生成店铺编号
    public static String getStoreId() {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "").substring(0, 5);
        return uuid;
    }

}
