package com.vip.common.bean;

public class FinalBean {

    public static final String COLSTORENUM = "colStoreNum:";

    public static final Integer PAGESIZE = 5;

    public static final String ORDERID = "orderId";

    public static final String TOKEN = "token";

    public static final String WTOKEN = "wToken";

    public static final String LOGINUSER = "loginUser";

    public static final Integer SEARCHSIZE = 10;

    public static final Integer CHANGENUM = 1;

}
